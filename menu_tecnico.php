<?php
$result = mysqli_query($cn,"SELECT orden_trabajo.Id,
       cliente.Codigo,
       cliente.Nombre,
       orden_trabajo.Descripcion,
       orden_trabajo.Codigo
  FROM orden_trabajo orden_trabajo
       INNER JOIN cliente cliente
          ON (orden_trabajo.Cliente = cliente.Id)
          where Estado >1 and orden_trabajo.FechaProgramada = CURDATE()");
$row_result_count = mysqli_num_rows($result);

?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>General de Equipos</title>

        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="vendors/nprogress/nprogress.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><span>General de Equipos</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="img/mecanico.png" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Bienvenido,</span>
                                <h2><?php echo $_SESSION['Usuario']; ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">

                                    <li><a><i class="fa fa-edit"></i> Orden Trabajo <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="control/orden_trabajo.php" target='contenido' >Orden Trabajo</a></li>
                                        </ul>
                                    </li>
                                  



                                </ul>
                            </div>


                        </div>
                        <!-- /sidebar menu -->

                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="img/mecanico.png" alt=""><?php echo $_SESSION['Usuario']; ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                       
                                        <li><a href="conexion/salir.php"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                                    </ul>
                                </li>

                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green"><?php echo $row_result_count; ?></span>
                                    </a>   
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                        <?php
                                        if ($row_result_count > 0) {
                                            while ($row_result = mysqli_fetch_array($result)) {
                                                ?>
                                                <li>
                                                    <a>
                                                        <a href="control/orden_detalle.php?Id=<?php echo $row_result[0]; ?>"target='contenido'><span class="image"><img src="img/engrane.png" alt="Profile Image" /></span>
                                                            <span>
                                                                <span><strong>Orden<?php echo ' ' . $row_result[4]; ?></strong></span>
                                                                <span class="time">Cliente<?php echo ' ' . $row_result[1]; ?></span>
                                                            </span>
                                                            <span class="message">
                                                                <?php echo $row_result[2]; ?>
                                                            </span>
                                                            <span class="message">
                                                                <?php echo $row_result[3]; ?>
                                                            </span>
                                                        </a>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <li>
                                            <div class="text-center">
                                                <a>
                                                    <strong>Ver todas las ordenes</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->





                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="x_content">	
                            <iframe  scrolling="yes" name='contenido' style='width: 100%; height: 400%; border:0; '  >

                            </iframe>

                        </div>	
                    </div>
                </div>
                <!-- /page content -->


            </div>
        </div>

        <!-- jQuery -->
        <script src="vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="vendors/nprogress/nprogress.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="build/js/custom.min.js"></script>
    </body>
</html>
