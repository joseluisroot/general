<html>
    <head>
        <meta charset="UTF-8">
        <title>General de equipos</title>
    </head>
    <body>
        <?php
        include("conexion/conexion_admin.php");
        if($_SESSION['Rol']==1)
        {
            include('menu_admin.php');
        }
        else if($_SESSION['Rol']==2)
        {
            include('menu_supervisor.php');
        }
		else if($_SESSION['Rol']==3)
        {
            include('menu_tecnico.php');
        }
		else if($_SESSION['Rol']==4)
        {
            include('menu_vendedor.php');
        }
        ?>
    </body>
</html>
