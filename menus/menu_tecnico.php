<?php
include('../conexion/conexion_admin.php');
$result = mysql_query("SELECT orden_trabajo.Id,
       cliente.Codigo,
       cliente.Nombre,
       orden_trabajo.Descripcion,
       orden_trabajo.Codigo
  FROM `general`.orden_trabajo orden_trabajo
       INNER JOIN `general`.cliente cliente
          ON (orden_trabajo.Cliente = cliente.Id)
          where orden_trabajo.FechaProgramada = CURDATE()");
$row_result_count = mysql_num_rows($result);
?>

<!DOCTYPE html>
<html lang="en">`
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>General de Equipos</title>

        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../build/css/custom.min.css" rel="stylesheet">
    </head>

    <body  class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><span>General de Equipos</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="../img/mecanico.png" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Bienvenido,</span>
                                <h2><?php echo $_SESSION['Usuario']; ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">

                                    <li><a><i class="fa fa-edit"></i> Orden Trabajo <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="../control/orden_trabajo.php" >Orden Trabajo</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-table"></i> Consultas<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="../control/historial_orden.php">Historial de ordenes</a></li>
                                            <!--<li><a href="../control/calendario.php">Calendarizacion</a></li>-->
                                        </ul>
                                    </li>



                                </ul>
                            </div>


                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="../img/mecanico.png" alt=""><?php echo $_SESSION['Usuario']; ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="javascript:;"> Profile</a></li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="badge bg-red pull-right">50%</span>
                                                <span>Settings</span>
                                            </a>
                                        </li>
                                        <li><a href="javascript:;">Help</a></li>
                                        <li><a href="../conexion/salir.php"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                                    </ul>
                                </li>




                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green"><?php echo $row_result_count; ?></span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                        <?php
                                        if ($row_result_count > 0) {
                                            while ($row_result = mysql_fetch_array($result)) {
                                                ?>
                                                <li>
                                                    <a>
                                                        <a href="../control/orden_detalle.php?Id=<?php echo $row_result[0]; ?>"><span class="image"><img src="../img/engrane.png" alt="Profile Image" /></span>
                                                        <span>
                                                            <span><strong>Orden<?php echo ' ' . $row_result[4]; ?></strong></span>
                                                            <span class="time">Cliente<?php echo ' ' . $row_result[1]; ?></span>
                                                        </span>
                                                        <span class="message">
                                                            <?php echo $row_result[2]; ?>
                                                        </span>
                                                        <span class="message">
                                                            <?php echo $row_result[3]; ?>
                                                        </span>
														</a>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <li>
                                            <div class="text-center">
                                                <a>
                                                    <strong>Ver todas las ordenes</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>




                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <div class="right_col" role="main">














