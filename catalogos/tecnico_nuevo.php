<?php
include('../conexion/conexion_usuario.php');
?>
<html>
    <head>
        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../build/css/custom.min.css" rel="stylesheet">

        <script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

        <script src="ajax.js" type="text/javascript"></script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tecnico Nuevo <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Codigo</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Codigo"  class="form-control" onBlur="this.value = this.value.toUpperCase();"  autofocus="true">

                                </div>

                            </div> 


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Primer Apellido</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="PrimerApellido"  class="form-control" onBlur="this.value = this.value.toUpperCase();"  autofocus="true">

                                </div>

                            </div> 

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Segundo Apellido</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="SegundoApellido"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">

                                </div>

                            </div> 


                            <div class="form-group">

                                <label class="col-sm-3 control-label">Nombre</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Nombre"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">

                                </div>

                            </div> 

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Gerencia</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Gerencia"  class="form-control" onBlur="this.value = this.value.toUpperCase();">
                                </div>
                            </div> 

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Unidad organizativa</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Unidad"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">
                                </div>
                            </div> 

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Posicion</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Posicion"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">
                                </div>
                            </div> 


                            <div class="form-group">

                                <label class="col-sm-3 control-label">Jefe</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Jefe"  class="form-control" onBlur="this.value = this.value.toUpperCase();">
                                </div>
                            </div> 




                            <div class="form-group">

                                <label class="col-sm-3 control-label">Telefono</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="Telefono"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Usuario</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="Usuario" id="Usuario" class="form-control selectpicker" data-live-search="true" title="Seleccionar..." >

                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT usuario.Id, usuario.Usuario FROM usuario usuario where usuario.Rol = 3");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 	


                            <div class="form-group">

                                <label class="col-sm-3 control-label">Estado</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="Estado" id="Estado" class="form-control">
                                        <option value="1">ACTIVO</option>
                                        <option value="0">INACTIVO</option>

                                    </select>
                                </div>

                            </div> 


                            <div class="form-group" align="center">
                                <button id="btn_aceptar" type="button" class="btn btn-info ">Aceptar</button>
                                &nbsp;&nbsp;&nbsp;
                                <a href="tecnico.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                            </div>

                            <div class="box-footer" id="targetDiv">

                            </div>



                        </form>
                    </div>
                </div>
            </div>
        </div>







        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
    </body>


    <script type="text/javascript">
                                        $(document).ready(function () {



                                            $('#btn_aceptar').click(function () {

                                                if (document.getElementById('Codigo').value.trim() == '') {
                                                    alert('Error!, Digite el Codigo');
                                                    return false;
                                                }

                                                if (document.getElementById('PrimerApellido').value.trim() == '') {
                                                    alert('Error!, Digite el Primer apellido');
                                                    return false;
                                                }                                                
                                                if (document.getElementById('Nombre').value.trim() == '') {
                                                    alert('Error!, Digite el Nombre');
                                                    return false;
                                                }
                                                if (document.getElementById('Gerencia').value.trim() == '') {
                                                    alert('Error!, Digite una Gerencia');
                                                    return false;
                                                }
                                                if (document.getElementById('Unidad').value.trim() == '') {
                                                    alert('Error!, Digite una Unidad organizativa');
                                                    return false;
                                                }
                                                if (document.getElementById('Posicion').value.trim() == '') {
                                                    alert('Error!, Digite una Posicion');
                                                    return false;
                                                }
                                                if (document.getElementById('Jefe').value.trim() == '') {
                                                    alert('Error!, Digite el Jefe');
                                                    return false;
                                                }

                                                $.post('tecnico_guardar.php',
                                                        {
                                                            Tipo: '1',
                                                            Codigo: document.getElementById('Codigo').value.trim(),
                                                            PrimerApellido: document.getElementById('PrimerApellido').value.trim(),
                                                            SegundoApellido: document.getElementById('SegundoApellido').value.trim(),
                                                            Nombre: document.getElementById('Nombre').value.trim(),
                                                            Gerencia: document.getElementById('Gerencia').value.trim(),
                                                            Unidad: document.getElementById('Unidad').value.trim(),
                                                            Posicion: document.getElementById('Posicion').value.trim(),
                                                            Jefe: document.getElementById('Jefe').value.trim(),
                                                            Telefono: document.getElementById('Telefono').value.trim(),
                                                            Usuario: document.getElementById('Usuario').value,
                                                            Estado: document.getElementById('Estado').value,

                                                        },
                                                        function (data, status) {
                                                            $('#targetDiv').html(data);
                                                            // alert(data);
                                                        });
                                                document.getElementById('Codigo').value = '';
                                                document.getElementById('PrimerApellido').value = '';
                                                document.getElementById('SegundoApellido').value = '';
                                                document.getElementById('Nombre').value = '';
                                                document.getElementById('Gerencia').value = '';                                                
                                                document.getElementById('Unidad').value = '';
                                                document.getElementById('Posicion').value = '';
                                                document.getElementById('Jefe').value = '';
                                                document.getElementById('Telefono').value = '';
                  

                                            });
                                        });



    </script>
</body>
</html>
