<?php
include('../conexion/conexion_admin.php');
$rs = mysqli_query($cn,"SELECT U.Id,u.Usuario,u.Clave,u.Correo,r.Rol,u.Estado FROM usuario U INNER JOIN Rol R ON U.Rol= R.Id");
$row_count = mysqli_num_rows($rs);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">
</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Usuarios <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>


                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Usuario</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Usuario" autofocus="true" onBlur="this.value = this.value.toUpperCase();" required="true" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Clave</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="Clave" required="true" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Correo" class="col-sm-2 control-label">Correo</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="Correo" required="true" id="Correo" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Rol</label>
                                <div class="col-sm-10">

                                    <select name="Rol" id="Rol" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn,"SELECT * FROM rol");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2["Id"]; ?>"><?php echo $row2["Rol"]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button name="btnAceptar" type="submit" class="btn btn-info pull-right">Aceptar</button>
                        </div>

                        <?php
                        if (isset($_POST["btnAceptar"])) {
                            
                            $sql = "insert into usuario (Usuario,Clave,Correo,Rol,Estado) values ('" . $_POST["Usuario"] . "','" . $_POST["Clave"] . "','".$_POST['Correo']."'," . $_POST["Rol"] . ",1)";
                            //$recurso = sqlsrv_prepare($sql);
                            
                            $resultado = mysqli_query($cn,$sql);
                            mysqli_close($cn);
                            if ($resultado) {
                                echo ' <div align="center" class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong> Registro creado correctamente.</strong></div>';
                                // header("Location: usuario.php");
                            } else {
                                echo ' <div align="center" class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong> No Agregado.</strong></div>';
                            }
                        }
                        ?>



                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Listado de usuarios<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><center>Id</center></th>
                        <th><center>Usuario</center></th>
                        <th><center>Correo</center></th>
                        <th><center>Rol</center></th>
                        <th><center>Estado</center></th>
                        <th><center>Modificar</center</th>
                        </tr>
                        </thead>


                        <tbody>
                            <?php
                            if ($row_count > 0) {

                                while ($row = mysqli_fetch_array($rs)) {
                                    ?>
                                    <tr align="center">
                                        <td><?php echo $row['Id']; ?></td>
                                        <td><?php echo $row['Usuario']; ?></td>
                                        <td><?php echo $row['Correo']; ?></td>
                                        <td><?php echo $row['Rol']; ?></td>
                                        <td><?php
                                            if ($row['Estado'] == 1) {
                                                echo 'ACTIVO';
                                            } else {
                                                echo 'INACTIVO';
                                            }
                                            ?></td>
                                        <td>
                                            <a href="usuario_modificar.php?Id=<?php echo $row['Id']; ?>">   <button  class="btn btn-success">Modificar</button></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                echo '<div align="center" class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               <strong>No existen registros!</strong> </div>';
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>
    </div>





    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>
</html>
