<?php
if (isset($_GET["Id"])) {
    include('../conexion/conexion_admin.php');
    $rs = mysqli_query($cn, "SELECT * FROM componente where Id = " . $_GET["Id"]);
    $row_count = mysqli_num_rows($rs);
    $row = mysqli_fetch_array($rs);
    ?>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>

    </head>
    <body>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>componentes <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>


                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Codigo</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="id" id="id"  class="form-control" value="<?php echo $row["Id"]; ?>" onBlur="this.value = this.value.toUpperCase();" required="true" >
                                    <input type="text" name="Codigo" id="Codigo" class="form-control" value="<?php echo $row["Codigo"]; ?>"  onBlur="this.value = this.value.toUpperCase();" required="true" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Descripcion</label>
                                <div class="col-sm-10">
                                    <input type="text" name="Descripcion" id="Descripcion" class="form-control" value="<?php echo $row["Descripcion"]; ?>"  onBlur="this.value = this.value.toUpperCase();" required="true" >
                                </div>
                            </div>
                            <div class="form-group" align="center">
                                <input type="button" name="btnModificar" id="btnModificar" value="Modificar" class="btn btn-success">&nbsp;&nbsp;&nbsp;
                                <input type="button" name="btnEliminar" id="btnEliminar" value="Eliminar" class="btn btn-danger" >&nbsp;&nbsp;&nbsp;
                                <a href="componente.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                            </div>

                            <div class="box-footer" id="targetDiv">

                            </div>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#btnModificar').click(function () {
                                        if (document.getElementById('Codigo').value.trim() == '') {
                                            alert('Error!, Digite un codigo');
                                            return false;
                                        }
                                        if (document.getElementById('Descripcion').value.trim() == '') {
                                            alert('Error!, Digite un Descripcion');
                                            return false;
                                        }


                                        $.post('componente_guardar.php',
                                                {
                                                    Tipo: '2',
                                                    Codigo: document.getElementById('Codigo').value.trim(),
                                                    Descripcion: document.getElementById('Descripcion').value.trim(),
                                                    Id: document.getElementById('id').value,

                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });


                                    });



                                    $('#btnEliminar').click(function () {
                                        if (confirm('Esta seguro que desea Eliminar este Registro?')) {
                                            if (document.getElementById('id').value == '') {
                                                alert('Error!, el registro no existe');
                                                return false;
                                            }
                                            $.post('componente_guardar.php',
                                                    {
                                                        Tipo: '3',
                                                        Id: document.getElementById('id').value,

                                                    },
                                                    function (data, status) {
                                                        $('#targetDiv').html(data);
                                                        //alert(data);
                                                    });

                                            document.getElementById('Descripcion').value = '';
                                            document.getElementById('Codigo').value = '';
                                            document.getElementById('id').value = '';
                                        }
                                    });


                                });
                            </script>


                        </form>

                        

                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>

    </body>
    </html>
    <?php
}
?>