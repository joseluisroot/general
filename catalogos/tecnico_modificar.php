<?php
include('../conexion/conexion_usuario.php');
$rs = mysqli_query($cn, "SELECT * FROM tecnico where Id = " . $_GET["Id"]);
$row_count = mysqli_num_rows($rs);
$row = mysqli_fetch_array($rs);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

<script src="ajax.js" type="text/javascript"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tecnico <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Codigo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="hidden"  id="id" value="<?php echo $row["Id"] ?>"  class="form-control">
                                <input type="text"  id="Codigo"  name="Codigo" value="<?php echo $row["Codigo"] ?>"  class="form-control" onBlur="this.value = this.value.toUpperCase();"  autofocus="true">

                            </div>

                        </div> 


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Primer Apellido</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="PrimerApellido"  value="<?php echo $row["PrimerApellido"] ?>" class="form-control" onBlur="this.value = this.value.toUpperCase();"  autofocus="true">

                            </div>

                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Segundo Apellido</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="SegundoApellido"  value="<?php echo $row["SegundoApellido"] ?>"  class="form-control"  onBlur="this.value = this.value.toUpperCase();">

                            </div>

                        </div> 


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Nombre</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Nombre" name="Nombre"  class="form-control"  value="<?php echo $row["Nombre"] ?>" onBlur="this.value = this.value.toUpperCase();">

                            </div>

                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Gerencia</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Gerencia"  class="form-control"  value="<?php echo $row["Gerencia"] ?>" onBlur="this.value = this.value.toUpperCase();">
                            </div>
                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Unidad organizativa</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Unidad"  class="form-control" value="<?php echo $row["Unidad"] ?>"   onBlur="this.value = this.value.toUpperCase();">
                            </div>
                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Posicion</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Posicion"  class="form-control"value="<?php echo $row["Posicion"] ?>"    onBlur="this.value = this.value.toUpperCase();">
                            </div>
                        </div> 


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Jefe</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Jefe"  class="form-control" value="<?php echo $row["Jefe"] ?>"   onBlur="this.value = this.value.toUpperCase();">
                            </div>
                        </div> 




                        <div class="form-group">

                            <label class="col-sm-3 control-label">Telefono</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Telefono"  class="form-control"  value="<?php echo $row["Telefono"] ?>" onBlur="this.value = this.value.toUpperCase();">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Usuario</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name="Usuario" id="Usuario" class="form-control selectpicker" data-live-search="true" title="Seleccionar..." >

                                    <?php
                                    $rs2 = mysqli_query($cn, "SELECT usuario.Id, usuario.Usuario FROM usuario usuario where usuario.Rol = 3");
                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                        ?>
                                        <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> 	


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="Estado" id="Estado" class="form-control">
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>

                                </select>
                            </div>

                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Firma</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file"  name="Firma"  class="form-control" >
                            </div>
                            <span class="input-group-btn">
                                <button type="submit" name="btn_firma"  class="btn btn-primary">Guardar Firma</button>
                            </span>
                        </div> 

                        <div class="form-group" align="center">
                            <?php
                            if ($row["Firma"] != '') {
                                echo '<img src="' . $row["Firma"] . '" width="100" height="100">';
                            }
                            ?>

                        </div>


                        <div class="form-group" align="center">
                            <input type="button" name="btnModificar" id="btnModificar" value="Modificar" class="btn btn-success">&nbsp;&nbsp;&nbsp;
                            <input type="button" name="btnEliminar" id="btnEliminar" value="Eliminar" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
                            <a href="tecnico.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                        </div>


                        <div class="box-footer" id="targetDiv">

                        </div>                           



                        <?php
                        if (isset($_POST["btn_firma"])) {
                            if ($_FILES['Firma']['name'] != '') {
                                $foto = $_FILES["Firma"]["name"];
                                $ruta = $_FILES["Firma"]["tmp_name"];

                                $foto = $_POST["Codigo"] . "_" . $_POST["Nombre"] . ".png";
                                $destino = "../img/" . $foto;

                                unlink($destino);
                                copy($ruta, $destino);
                                $sql = "update  tecnico set Firma = '$destino' where Id = " . $_GET["Id"];

                                $resultado = mysqli_query($cn, $sql);
                                //echo $sql;
                                if ($resultado) {

                                    echo ' <div align="center" class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <strong> La firma fue cargada con exito.</strong></div>';
                                } else {
                                    echo ' <div align="center" class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong> No Modificado.</strong></div>';
                                }
                            } else {
                                echo 'Error! se debe seleccionar una firma';
                            }
                        }
                        ?>

                    </form>
                </div>
            </div>
        </div>
    </div>







    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>


<script type="text/javascript">
                                    $(document).ready(function () {



                                        $('#btnModificar').click(function () {

                                            if (document.getElementById('Codigo').value.trim() == '') {
                                                alert('Error!, Digite el Codigo');
                                                return false;
                                            }

                                            if (document.getElementById('PrimerApellido').value.trim() == '') {
                                                alert('Error!, Digite el Primer apellido');
                                                return false;
                                            }
                                           
                                            if (document.getElementById('Nombre').value.trim() == '') {
                                                alert('Error!, Digite el Nombre');
                                                return false;
                                            }
                                            if (document.getElementById('Gerencia').value.trim() == '') {
                                                alert('Error!, Digite una Gerencia');
                                                return false;
                                            }
                                            if (document.getElementById('Unidad').value.trim() == '') {
                                                alert('Error!, Digite una Unidad organizativa');
                                                return false;
                                            }
                                            if (document.getElementById('Posicion').value.trim() == '') {
                                                alert('Error!, Digite una Posicion');
                                                return false;
                                            }
                                            if (document.getElementById('Jefe').value.trim() == '') {
                                                alert('Error!, Digite el Jefe');
                                                return false;
                                            }

                                            $.post('tecnico_guardar.php',
                                                    {
                                                        Tipo: '2',
                                                        Codigo: document.getElementById('Codigo').value.trim(),
                                                        PrimerApellido: document.getElementById('PrimerApellido').value.trim(),
                                                        SegundoApellido: document.getElementById('SegundoApellido').value.trim(),
                                                        Nombre: document.getElementById('Nombre').value.trim(),
                                                        Gerencia: document.getElementById('Gerencia').value.trim(),
                                                        Unidad: document.getElementById('Unidad').value.trim(),
                                                        Posicion: document.getElementById('Posicion').value.trim(),
                                                        Jefe: document.getElementById('Jefe').value.trim(),
                                                        Telefono: document.getElementById('Telefono').value.trim(),
                                                        Usuario: document.getElementById('Usuario').value,
                                                        Estado: document.getElementById('Estado').value,
                                                        Id: document.getElementById('id').value,
                                                    },
                                                    function (data, status) {
                                                        $('#targetDiv').html(data);
                                                        // alert(data);
                                                    });



                                        });

                                        $('#btnEliminar').click(function () {
                                            if (confirm('Esta seguro que desea Eliminar este Registro?')) {
                                                if (document.getElementById('id').value.trim() == '') {
                                                    alert('Error!, el registro no existe');
                                                    return false;
                                                }

                                                $.post('tecnico_guardar.php',
                                                        {
                                                            Tipo: '3',
                                                            Id: document.getElementById('id').value,

                                                        },
                                                        function (data, status) {
                                                            $('#targetDiv').html(data);
                                                            //alert(data);
                                                        });

                                                document.getElementById('Codigo').value = '';
                                                document.getElementById('PrimerApellido').value = '';
                                                document.getElementById('SegundoApellido').value = '';
                                                document.getElementById('Nombre').value = '';
                                                document.getElementById('Gerencia').value = '';
                                                document.getElementById('Unidad').value = '';
                                                document.getElementById('Posicion').value = '';
                                                document.getElementById('Jefe').value = '';
                                                document.getElementById('Telefono').value = '';
                                                document.getElementById('id').value = '';
                                            }
                                        });


                                    });



</script>
</html>
