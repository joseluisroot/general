<?php
include('../conexion/conexion_usuario.php');
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

<script src="ajax.js" type="text/javascript"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>usuario Nuevo <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Nombre</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Nombre"  class="form-control"  onBlur="this.value = this.value.toUpperCase();" autofocus="true">

                            </div>

                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Apellidos</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Apellido"  class="form-control" onBlur="this.value = this.value.toUpperCase();"  >

                            </div>

                        </div> 





                        <div class="form-group">

                            <label class="col-sm-3 control-label">Usuario</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Usuario"  class="form-control">
                            </div>
                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Clave</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password"  id="Clave"  class="form-control" >
                            </div>
                        </div> 


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rol</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name="Rol" id="Rol" class="form-control selectpicker" data-live-search="true" title="Seleccionar..." >

                                    <?php
                                    $rs2 = mysqli_query($cn, "SELECT * FROM rol");
                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                        ?>
                                        <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> 	

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Correo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Correo"  class="form-control" >
                            </div>
                        </div> 


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Telefono</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Telefono"  class="form-control" >
                            </div>
                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="Estado" id="Estado" class="form-control">
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>

                                </select>
                            </div>

                        </div> 


                        <div class="form-group" align="center">
                            <button id="btn_aceptar" type="button" class="btn btn-info ">Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <a href="usuario.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                        </div>

                        <div class="box-footer" id="targetDiv">

                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>







    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>


<script type="text/javascript">
                                    $(document).ready(function () {



                                        $('#btn_aceptar').click(function () {

                                            if (document.getElementById('Usuario').value.trim() == '') {
                                                alert('Error!, Digite el Usuario');
                                                return false;
                                            }

                                            if (document.getElementById('Clave').value.trim() == '') {
                                                alert('Error!, Digite la Clave');
                                                return false;
                                            }

                                            $.post('usuario_guardar.php',
                                                    {

                                                        Tipo: '1',
                                                        Nombre: document.getElementById('Nombre').value.trim(),
                                                        Apellido: document.getElementById('Apellido').value.trim(),
                                                        Usuario: document.getElementById('Usuario').value.trim(),
                                                        Clave: document.getElementById('Clave').value.trim(),
                                                        Telefono: document.getElementById('Telefono').value.trim(),
                                                        Correo: document.getElementById('Correo').value.trim(),
                                                        Rol: document.getElementById('Rol').value,
                                                        Estado: document.getElementById('Estado').value,

                                                    },
                                                    function (data, status) {
                                                        $('#targetDiv').html(data);
                                                        //alert(data);
                                                    });

                                            document.getElementById('Nombre').value = '';
                                            document.getElementById('Apellido').value = '';
                                            document.getElementById('Usuario').value = '';
                                            document.getElementById('Clave').value = '';
                                            document.getElementById('Telefono').value = '';
                                            document.getElementById('Correo').value = '';


                                        });
                                    });



</script>
</html>
