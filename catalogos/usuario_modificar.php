<?php
if (isset($_GET["Id"])) {
    include('../conexion/conexion_admin.php');
    $rs = mysqli_query($cn, "SELECT * FROM usuario u where u.Id = " . $_GET["Id"]);
    $row_count = mysqli_num_rows($rs);
    $row = mysqli_fetch_array($rs);
    ?>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

    </head>
    <body>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Usuarios <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>


                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Usuario</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" value="<?php echo $row["Id"]; ?>" id="id"  required="true" >
                                    <input type="text" id="Usuario"  value="<?php echo $row["Usuario"]; ?>"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Clave</label>
                                <div class="col-sm-10">
                                    <input type="password" id="Clave"  value="<?php echo $row["Clave"]; ?>"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" id="Nombre"  value="<?php echo $row["Nombres"]; ?>" onBlur="this.value = this.value.toUpperCase();"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Apellido</label>
                                <div class="col-sm-10">
                                    <input type="text" id="Apellido"  value="<?php echo $row["Apellidos"]; ?>" onBlur="this.value = this.value.toUpperCase();"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Telefono</label>
                                <div class="col-sm-10">
                                    <input type="text" id="Telefono"  value="<?php echo $row["Telefono"]; ?>"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="Correo" class="col-sm-2 control-label">Correo</label>
                                <div class="col-sm-10">
                                    <input type="text" id="Correo"  value="<?php echo $row["Correo"]; ?>"  class="form-control">
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Rol</label>
                                <div class="col-sm-10">

                                    <select name="Rol" id="Rol" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM rol");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2["Id"]; ?>" <?php
                                            if ($row["Rol"] == $row2["Id"]) {
                                                echo 'selected';
                                            }
                                            ?>><?php echo $row2["Rol"]; ?></option> 
                                                    <?php
                                                }
                                                ?>
                                    </select>


                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Estado</label>
                                <div class="col-sm-10">
                                    <select name="Estado" id="Estado" class="form-control">
                                        <option value="1"<?php
                                        if ($row["Estado"] == 1) {
                                            echo 'selected';
                                        }
                                        ?>>ACTIVO</option>
                                        <option value="0"<?php
                                        if ($row["Estado"] == 0) {
                                            echo 'selected';
                                        }
                                        ?>>INACTIVO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" align="center">
                                <input type="button" id="btnModificar" value="Modificar" class="btn btn-success">&nbsp;&nbsp;&nbsp;
                                <input type="button" id="btnEliminar" value="Eliminar" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
                                <a href="usuario.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                            </div>
                        </form>

                        <div class="box-footer" id="targetDiv">

                        </div> 


                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>

    </body>

    <script type="text/javascript">
                                        $(document).ready(function () {

                                            $('#btnModificar').click(function () {

                                                if (document.getElementById('Usuario').value.trim() == '') {
                                                    alert('Error!, Digite el Usuario');
                                                    return false;
                                                }

                                                if (document.getElementById('Clave').value.trim() == '') {
                                                    alert('Error!, Digite la Clave');
                                                    return false;
                                                }

                                                $.post('usuario_guardar.php',
                                                        {
                                                            Tipo: '2',
                                                            Nombre: document.getElementById('Nombre').value.trim(),
                                                            Apellido: document.getElementById('Apellido').value.trim(),
                                                            Usuario: document.getElementById('Usuario').value.trim(),
                                                            Clave: document.getElementById('Clave').value.trim(),
                                                            Telefono: document.getElementById('Telefono').value.trim(),
                                                            Correo: document.getElementById('Correo').value.trim(),
                                                            Rol: document.getElementById('Rol').value,
                                                            Estado: document.getElementById('Estado').value,
                                                            Id: document.getElementById('id').value,

                                                        },
                                                        function (data, status) {
                                                            $('#targetDiv').html(data);
                                                            //alert(data);
                                                        });


                                            });


                                            $('#btnEliminar').click(function () {

                                                if (confirm('Esta seguro que desea Eliminar este Registro?')) {

                                                    if (document.getElementById('id').value == '') {
                                                        alert('Error!, el registro no existe');
                                                        return false;
                                                    }
                                                    $.post('usuario_guardar.php',
                                                            {
                                                                Tipo: '3',
                                                                Id: document.getElementById('id').value,

                                                            },
                                                            function (data, status) {
                                                                $('#targetDiv').html(data);
                                                                //alert(data);
                                                            });


                                                    document.getElementById('Nombre').value = '';
                                                    document.getElementById('Apellido').value = '';
                                                    document.getElementById('Usuario').value = '';
                                                    document.getElementById('Clave').value = '';
                                                    document.getElementById('Telefono').value = '';
                                                    document.getElementById('Correo').value = '';
                                                    document.getElementById('id').value = '';

                                                }
                                            });







                                        });



    </script>
    </html>
    <?php
}
?>