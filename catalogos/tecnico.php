<?php
include('../conexion/conexion_usuario.php');
$rs = mysqli_query($cn,"SELECT tecnico.Id,
       tecnico.Codigo,
       tecnico.PrimerApellido,
       tecnico.SegundoApellido,
       tecnico.Nombre,
       tecnico.Gerencia,
       tecnico.Unidad,
       tecnico.Posicion,
       tecnico.Jefe,
       tecnico.Telefono,
       usuario.Usuario,
       tecnico.Estado
  FROM tecnico tecnico
       LEFT JOIN usuario usuario ON (tecnico.Usuario = usuario.Id)");


$row_count = mysqli_num_rows($rs);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">




    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Listado de tecnicos <small></small> <a href="tecnico_nuevo.php"><button  class="btn btn-warning">Nuevo tecnico</button></a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><center>Cod</center></th>
                        <th><center>Nombre</center></th>
                        <th><center>Gerencia</center></th>
                        <th><center>Unidad</center></th>
                        <th><center>Posicion</center></th>
                        <th><center>Jefe</center></th>
                        <th><center>Telefono</center></th>
                     <th><center>Usuario</center></th>
                        <th><center>Estado</center></th>
                        <th><center>Modificar</center></th>
                        </tr>
                        </thead>


                        <tbody>
                            <?php
                            if ($row_count > 0) {

                                while ($row = mysqli_fetch_array($rs)) {
                                    ?>
                                    <tr align="center">
                                        <td><?php echo $row[1]; ?></td>
                                        <td><?php echo $row[2].' '.$row[3].' '.$row[4]; ?> </td>
                                        <td><?php echo $row[5]; ?></td>
                                        <td><?php echo $row[6]; ?></td>
                                        <td><?php echo $row[7]; ?></td>                                            
                                        <td><?php echo $row[8]; ?></td>
                                        <td><?php echo $row[9]; ?></td>
                                        <td><?php echo $row[10]; ?></td>
                                        <td><?php if($row[11]==1){ echo 'ACTIVO' ;} else { echo 'INACTIVO' ;}?></td>
                                        <td>
                                            <a href="tecnico_modificar.php?Id=<?php echo $row[0]; ?>">   <button  class="btn btn-success">Modificar</button></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                echo '<div align="center" class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               <strong>No existen registros!</strong> </div>';
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>
    </div>





    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>
</html>
