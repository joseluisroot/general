<?php
if (isset($_GET["Id"])) {
    include('../conexion/conexion_admin.php');
    $rs = mysqli_query($cn,"SELECT * FROM orden_trabajo where Id = " . $_GET["Id"]);
    $row_count = mysqli_num_rows($rs);
    echo $row_count;
    $row = mysqli_fetch_array($rs);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../build/css/custom.min.css" rel="stylesheet">

        <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>marcas <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>


                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Codigo</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="Codigo" id="Codigo"  class="form-control" required="true" onBlur="this.value = this.value.toUpperCase();" autofocus="true">
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Nombre</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="Nombre" id="Nombre" class="form-control" required="true" onBlur="this.value = this.value.toUpperCase();" .. >
                                    </div>
                                </div>
                            </div>


                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button id="btn_aceptar" type="button" class="btn btn-info pull-right">Aceptar</button>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer" id="targetDiv">

                            </div>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#btn_aceptar3').click(function () {
                                        if (document.getElementById('Codigo').value == '') {
                                            alert('Error!, Digite un codigo');
                                            return false;
                                        }
                                        if (document.getElementById('Nombre').value == '') {
                                            alert('Error!, Digite un Nombre');
                                            return false;
                                        }


                                        $.post('marca_guardar.php',
                                                {
                                                    Tipo: '1',
                                                    Codigo: document.getElementById('Codigo').value,
                                                    Nombre: document.getElementById('Nombre').value,

                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });


                                    });
                                });
                            </script>

                        </form>
                    </div>
                </div>
            </div>
        </div>















        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- bootstrap-progressbar -->
        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
    </body>
</html>
