<?php
include('../conexion/conexion_admin.php');
$rs = mysqli_query($cn, "SELECT orden.Id,
       orden.Codigo AS CodOrden,
       cliente.Codigo AS CodCliente,
       cliente.Nombre AS Cliente,
       tecnico.Codigo AS CodTecnico,
       tecnico.Nombre AS Tecnico,
       marca.Codigo AS CodMarca,
       marca.Nombre AS Marca,
       orden.FechaProgramada,
       orden.Descripcion,
       orden.Serie,
       orden.Modelo,
       estado.Descripcion AS Estado,
        orden.FechaApertura
  FROM (((`general`.orden_trabajo orden
          INNER JOIN `general`.marca marca ON (orden.Marca = marca.Id))
         INNER JOIN `general`.cliente cliente ON (orden.Cliente = cliente.Id))
        LEFT JOIN `general`.tecnico tecnico ON (orden.Tecnico = tecnico.Id))
       INNER JOIN `general`.estado estado ON (orden.Estado = estado.Id)");
$row_count = mysqli_num_rows($rs);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Listado de ordenes de trabajo <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><center>Cod</center></th>
                        <th><center>Fecha Creacion</center></th>
                        <th><center>Cliente</center></th>
                        <th><center>Tecnico</center></th>
                        <th><center>Marca</center></th>                        

                        <th><center>Serie</center></th>
                        <th><center>Modelo</center></th>
                        <th><center>Fecha Programada</center></th>
                        <th><center>Editar</center></th>  
                        <th><center> <?php
                            if ($_SESSION['Rol'] == 1 || $_SESSION['Rol'] == 2) {
                                echo 'Asignar';
                            } else if ($_SESSION['Rol'] == 3) {
                                echo 'Iniciar';
                            }
                            ?></center</th>
                        </tr>
                        </thead>


                        <tbody>
                            <?php
                            if ($row_count > 0) {
                                while ($row = mysqli_fetch_array($rs)) {
                                    ?>
                                    <tr align="center">
                                        <td><?php echo $row[1]; ?></td>
                                        <td><?php echo $row[13]; ?></td>
                                        <td><?php echo $row[2]; ?></td>
                                        <td><?php echo $row[4]; ?></td>
                                        <!--<td><?php //echo $row[6];   ?></td>  -->                                      
                                        <td><?php echo $row[9]; ?></td>
                                        <td><?php echo $row[10]; ?></td>
                                        <td><?php echo $row[11]; ?></td>
                                        <td><?php echo $row[8]; ?></td>
                                        <td><?php echo $row[12]; ?></td>
                                        <td>
                                            <a href="orden_modificar.php?Id=<?php echo $row['Id']; ?>">
                                                <i class="fa fa-edit"></i> Editar
                                                </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                echo '<div align="center" class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               <strong>No existen registros!</strong> </div>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>



        </div>
    </div>





    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>
</html>
