<?php
include('../conexion/conexion_usuario.php');
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

<script src="ajax.js" type="text/javascript"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cliente Nuevo <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Codigo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Codigo"  class="form-control"   autofocus="true">

                            </div>

                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Nombre"  class="form-control" onBlur="this.value = this.value.toUpperCase();"  >

                            </div>

                        </div> 





                        <div class="form-group">

                            <label class="col-sm-3 control-label">Telefonos</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Telefonos"  class="form-control">
                            </div>
                        </div> 

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Correo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Correo"  class="form-control" >
                            </div>
                        </div> 


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Direccion</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Direccion"  class="form-control" >
                            </div>
                        </div> 	

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Sitio web</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  id="Sitioweb"  class="form-control" >
                            </div>
                        </div> 

                        <div class="form-group" align="center">
                            <button id="btn_aceptar" type="button" class="btn btn-info ">Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <a href="cliente.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                        </div>

                        <div class="box-footer" id="targetDiv">

                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>







    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>


<script type="text/javascript">
                                    $(document).ready(function () {



                                        $('#btn_aceptar').click(function () {

                                            if (document.getElementById('Codigo').value.trim() == '') {
                                                alert('Error!, Digite el Codigo');
                                                return false;
                                            }

                                            if (document.getElementById('Nombre').value.trim() == '') {
                                                alert('Error!, Digite el Nombre');
                                                return false;
                                            }

                                            $.post('cliente_guardar.php',
                                                    {

                                                        Tipo: '1',
                                                        Nombre: document.getElementById('Nombre').value.trim(),
                                                        Codigo: document.getElementById('Codigo').value.trim(),
                                                        Telefonos: document.getElementById('Telefonos').value,
                                                        Correo: document.getElementById('Correo').value.trim(),
                                                        Direccion: document.getElementById('Direccion').value.trim(),
                                                        Sitioweb: document.getElementById('Sitioweb').value.trim(),
                                                    },
                                                    function (data, status) {
                                                        $('#targetDiv').html(data);
                                                        //alert(data);
                                                    });

                                            document.getElementById('Nombre').value = '';
                                            document.getElementById('Codigo').value = '';
                                            document.getElementById('Telefonos').value = '';
                                            document.getElementById('Correo').value = '';
                                            document.getElementById('Direccion').value = '';
                                            document.getElementById('Sitioweb').value = '';


                                        });
                                    });



</script>
</html>
