

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../build/css/custom.min.css" rel="stylesheet">

    </head>
    <body>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Importar ordenes de trabajo</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>                          

                        </ul>
                        <div class="clearfix"></div>
                    </div>


                    <?php
                    set_time_limit(0);

                    //echo "inicio desde importar 2<br>";





                    // cargamos el archivo al servidor con el mismo nombre
                    // solo le agregue el sufijo bak_
                    $archivo = $_FILES['excel']['name'];
                    $tipo = $_FILES['excel']['type'];
                    $destino = "bak_" . $archivo;
                    if (copy($_FILES['excel']['tmp_name'], $destino))
                        echo "Archivo Cargado Con Éxito<br><hr>";
                    else
                        echo "Error Al Cargar el Archivo";
                    ////////////////////////////////////////////////////////
                    if (file_exists("bak_" . $archivo)) {
                        /** Clases necesarias */
                        require_once('Classes/PHPExcel.php');
                        require_once('Classes/PHPExcel/Reader/Excel2007.php');

                        // Cargando la hoja de cálculo
                        $objReader = new PHPExcel_Reader_Excel2007();
                        $objPHPExcel = $objReader->load("bak_" . $archivo);
                        $objFecha = new PHPExcel_Shared_Date();

                        // Asignar hoja de excel activa
                        $objPHPExcel->setActiveSheetIndex(0);




                        $i = 2;
                        while ($i >= 2) {
                            $_DATOS_EXCEL[$i]['wono'] = $objPHPExcel->getActiveSheet()->getCell('A' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['acti'] = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['f_apertura'] = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['respar'] = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['cuno'] = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['cunm'] = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['marca'] = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['modelo'] = $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['serie'] = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['no_eq'] = $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['slnm'] = $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['esbynm'] = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getCalculatedValue();
                            $_DATOS_EXCEL[$i]['notas'] = $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getCalculatedValue();


                            $wono = $_DATOS_EXCEL[$i]['wono'];
                            $acti = $_DATOS_EXCEL[$i]['acti'];
                            $f_apertura = $_DATOS_EXCEL[$i]['f_apertura'];
                            $respar = $_DATOS_EXCEL[$i]['respar'];
                            $cuno = $_DATOS_EXCEL[$i]['cuno'];
                            $cunm = $_DATOS_EXCEL[$i]['cunm'];
                            $marca = $_DATOS_EXCEL[$i]['marca'];
                            $modelo = $_DATOS_EXCEL[$i]['modelo'];
                            $serie = $_DATOS_EXCEL[$i]['serie'];
                            $no_eq = $_DATOS_EXCEL[$i]['no_eq'];
                            $slnm = $_DATOS_EXCEL[$i]['slnm'];
                            $esbynm = $_DATOS_EXCEL[$i]['esbynm'];
                            $notas = $_DATOS_EXCEL[$i]['notas'];

                            $acti = 1;

                            //conectamos con la base de datos
                            $link = mysqli_connect("localhost", "root", "", "general");

                            //busando codigo de orden
                            $sql = "SELECT ifnull(count(*),0) as cuenta FROM orden_trabajo o where Codigo='$wono'";
                            $result = mysqli_query($link, $sql);
                            while ($row = $result->fetch_assoc()) {
                                $cuenta = $row['cuenta'];
                            }

                            if ($cuenta > 0) {
                                echo "<span style='color:red'>Codigo Orden $wono ya existe en sistema</span><br>";
                            } else {
                                //busando codigo de cliente
                                $id_cliente = 0;
                                $sql = "SELECT ifnull(Id,'') as id_cliente FROM cliente c where Codigo='$cuno'";
                                $result = mysqli_query($link, $sql);
                                if ($result) {
                                    while ($row = $result->fetch_assoc()) {
                                        $id_cliente = $row['id_cliente'];
                                    }
                                }


                                if ($id_cliente != 0) {
                                    //busando codigo de marca
                                    $id_marca = 0;
                                    $sql = "SELECT ifnull(Id,0) as id_marca FROM marca m where codigo='$marca'";
                                    $result = mysqli_query($link, $sql);
                                    if ($result) {
                                        while ($row = $result->fetch_assoc()) {
                                            $id_marca = $row['id_marca'];
                                        }
                                    }


                                    // if ($id_marca!=0) {
                                    //   echo "Codigo Marca $marca tiene Id = $id_marca<br>";
                                    // } else {
                                    //   echo "Codigo Marca $marca NO existe<br>";
                                    // }


                                    $sql = "insert into orden_trabajo(
          Codigo,
          Cliente,
          Marca,
          FechaApertura,
          Descripcion,
          Serie,
          Modelo,
          Estado,
          Responsable,
          Equipo,
          Asesor,
          Elaborado
          )values(
            '$wono',
            '$id_cliente',
            '$id_marca',
            '$f_apertura',
            '$notas',
            '$serie',
            '$modelo',
            '$acti',
            '$respar',
            '$no_eq',
            '$slnm',
            '$esbynm'
            )";

                                    $result = mysqli_query($link, $sql);

                                    if (!$result) {
                                        echo "<span style='color:red'>Codigo de Orden $wono NO pudo ser ingresada</span><br>";
                                    } else {
                                        echo "<span style='color:green'>Codigo de Orden $wono importada correctamente</span><br>";
                                    }
                                    //echo "Codigo Cliente $cuno tiene Id = $id_cliente<br>";
                                } else {
                                    echo "<span style='color:blue'>Codigo Cliente $cuno -> $cunm NO existe</span><br>";
                                }
                                //echo "Codigo Orden $wono NO existe<br>";
                            }


                            mysqli_close($link);

                            if ($wono == '') {
                                $i = 0;
                            } else {
                                $i++;
                            }
                        }
                        // Llenamos el arreglo con los datos  del archivo xlsx
                        // for ($i=2;$i<=47;$i++){
                        //
  // }
                    }
                    //si por algo no cargo el archivo bak_
                    else {
                        echo "Necesitas primero importar el archivo";
                    }
                    $errores = 0;
                    //recorremos el arreglo multidimensional
                    //para ir recuperando los datos obtenidos
                    //del excel e ir insertandolos en la BD
                    // foreach($_DATOS_EXCEL as $campo => $valor){
                    // 	$sql = "INSERT INTO tmp_ordenes VALUES ('";
                    // 	foreach ($valor as $campo2 => $valor2){
                    // 		$campo2 == "esbynm" ? $sql.= $valor2."');" : $sql.= $valor2."','";
                    // 	}
                    // 	$result = mysql_query($sql);
                    //   echo $sql."<br>";
                    // 	if (!$result){ echo "Error al insertar registro ".$campo;$errores+=1;}
                    // }
                    /////////////////////////////////////////////////////////////////////////
                    //echo "<strong><center>ARCHIVO IMPORTADO CON EXITO, EN TOTAL $campo REGISTROS Y $errores ERRORES</center></strong>";
                    //una vez terminado el proceso borramos el
                    //archivo que esta en el servidor el bak_
                    unlink($destino);


                    echo "fin desde importar 2<br>";
                    ?>

                </div>
            </div>       

    </body>
</html>