<?php
include('../conexion/conexion_usuario.php');
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="../vendors/js/jquery-1.11.2.min.js" type="text/javascript"></script>

<script src="ajax.js" type="text/javascript"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Programacion de trabajo<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fecha</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="date"  id="Fecha"  class="form-control">

                            </div>

                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo actividad</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="TipoActividad" class="form-control">
                                    <option value="0">SELECCIONAR</option>
                                    <option value="1">VISITA CLIENTE</option>
                                    <option value="2">ACTIVIDAD COGESA</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cliente</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="Cliente" class="form-control">
                                    <option value="0">SELECCIONAR</option>
                                    <option value="1">VISITA CLIENTE</option>
                                    <option value="2">ACTIVIDAD COGESA</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                           <albel claslabels="col-sm-3 control-label">Hora inicio</label>
                            <div class='input-group date' id='myDatepicker3'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Hora final</label>
                            <div class='input-group date' id='myDatepicker3'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div> 

                        <div class="form-group" align="center">
                            <button id="btn_aceptar" type="button" class="btn btn-info ">Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btn_guardar" type="button" class="btn btn-warning">Guardar</button>
                        </div>

                        <div class="box-footer" id="targetDiv">

                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>






    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="../vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="../vendors/cropper/dist/cropper.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>


<!-- Initialize datetimepicker -->
<script>
    $('#myDatepicker').datetimepicker();

    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });

    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();

    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });

    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });

    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>




<script type="text/javascript">
    $(document).ready(function () {



        $('#btn_aceptar').click(function () {


            if (document.getElementById('Codigo').value == '') {
                alert('Error!, Digite el Codigo');
                return false;
            }
            if (document.getElementById('Descripcion').value == '') {
                alert('Error!, Digite el Descripcion');
                return false;
            }

            $.post('Componente_guardar.php',
                    {

                        Tipo: '1',
                        Descripcion: document.getElementById('Descripcion').value,
                        Codigo: document.getElementById('Codigo').value,
                    },
                    function (data, status) {
                        $('#targetDiv').html(data);
                        //alert(data);
                    });

            document.getElementById('Descripcion').value = '';
            document.getElementById('Codigo').value = '';
        });
    });



</script>
</html>
