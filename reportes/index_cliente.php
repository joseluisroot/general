<!DOCTYPE html>
<html lang="en">
  <head>
      
    <title>Reporte</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    
  </head>
  <body>
   <div class="container">
       <div style="background-color: black; height: 30px;">
           
       </div>
       <div style="background-color: darkorange; height: 15px;">
           
       </div>
     <div class="row">
        <div class="col col-lg-12" style="text-align: right;">
            <h1 style="padding: 0;margin-bottom: 0;"><span style="color: darkorange;">REPORTE</span> DE SERVICIO</h1>
            <h3 style="margin-top: 0;">COMPAÑIA GENERAL DE EQUIPOS</h3>
        </div>
     </div>  
       
     <div class="row">
       <div class="col col-lg-12">
         <h1 class="encabezado">SERVICIO</h1>
         <table class="table table-condensed">
           <tr>
             <td>Componentes principales:</td>
             <td>#componentesPrincipales</td>
           </tr>
           <tr>
             <td>Sub- sistemas:</td>
             <td>#subSistema</td>
           </tr>
           <tr>
             <td>Síntomas reportados</td>
             <td>#sintomaReportados</td>
           </tr>
           <tr>
             <td>Selección de componentes y operaciones:</td>
             <td>#seleccionComponentesOperaciones</td>
           </tr>
           <tr>
             <td>Pruebas realizadas:</td>
             <td>#pruebasRealizadas</td>
           </tr>
           <tr>
             <td>Calibraciones y ajustes realizados:</td>
             <td>#calibracionesAjustesRealizados</td>
           </tr>
           <tr>
             <td>Mediciones obtenidas:</td>
             <td>#medicionesObtenidas</td>
           </tr>
           <tr>
             <td>Análisis de fluidos:</td>
             <td>#analisisFluidos</td>
           </tr>
           <tr>
             <td>Condiciones de las piezas o componentes:</td>
             <td>#condicionesPiezasComponentes</td>
           </tr>
           <tr>
             <td>Descripción complementaria del servicio:</td>
             <td>#descripcioncomplementariaServicio</td>
           </tr>
           <tr>
             <td>Códigos de fallas:</td>
             <td>#codigosFallas</td>
           </tr>
           <tr>
             <td>Diagnóstico de la falla:</td>
             <td>#diagnosticoFalla</td>
           </tr>
           <tr>
             <td>Observaciones:</td>
             <td>#observaciones</td>
           </tr>
         </table>
       </div>
     </div>
     <div class="row ">
       <div class="col col-lg-12">
         <h1 class="encabezado">OTROS</h1>
         <table class="table table-condensed">
           <tr>
             <td>Condición del servicio:</td>
             <td>#condicionServicio</td>
           </tr>
           <tr>
             <td>Condición del equipo:</td>
             <td>#condicionEquipo</td>
           </tr>
           <tr>
             <td>Nombre del cliente:</td>
             <td>#nombre</td>
           </tr>
           <tr>
             <td>Firma del cliente:</td>
             <td>#firmaCliente</td>
           </tr>
           <tr>
             <td>Firma del técnico:</td>
             <td>#fimaTecnico</td>
           </tr>
           <tr>
             <td>Nombre del supervisor:</td>
             <td>#nombreSupervisor</td>
           </tr>
           <tr>
             <td>Firma del supervisor:</td>
             <td></td>
           </tr>
           <tr>
             <td>Teléfono del supervisor:</td>
             <td>#telefonoSuperviso</td>
           </tr>
           <tr>
             <td>Número correlativo de reportes:</td>
             <td>#correlativo</td>
           </tr>
         </table>
       </div>
     </div>
       
       <div class="row">
           <div class="col col-lg-6 center-block center" style="height: 60px">
               <img src="../control/fotos/firma_298_1517795062.png" alt="firmaCliente" style="display: block;margin: 0 auto">
               <p style="text-align: center">Cliente</p>
           </div>
           <div class="col col-lg-6 center-block center" style="height: 60px">
               <img src="../control/fotos/firma_298_1517795062.png" alt="firmaTecnico" style="display: block;margin: 0 auto">
               <p style="text-align: center">Técnico</p>
           </div>
       </div>
    
   </div>

    
  </body>
</html>