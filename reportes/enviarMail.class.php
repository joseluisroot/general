<?php

require_once __DIR__ . '/plugins/phpmailer/vendor/autoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


/**
 * Description of enviarMail
 *
 * @author joseluisroot
 */
class enviarMail {
    
    private $_correoDestinatario;
    private $_nombreDestinatario;
    private $_asunto;
    Private $_textoNotifiacion;
    private $_cn;
    
    private $_adjunto;
    private $_nombreAdjunto;
    
    private $_correoSup;
    
    private $_host;
    private $_userName;
    private $_password;
    private $_SMTPSecure;
    private $_puerto;
    
    
    
            
    function __construct($cn, $_correoDestinatario, $_nombreDestinatario, $_asunto, $_textoNotifiacion, $_adjunto=false, $_nombreAdjunto="", $_correoSup="") {
        $this->_correoDestinatario = $_correoDestinatario;
        $this->_nombreDestinatario = $_nombreDestinatario;
        $this->_asunto = $_asunto;
        $this->_textoNotifiacion = $_textoNotifiacion;
        $this->_adjunto = $_adjunto;
        $this->_nombreAdjunto = $_nombreAdjunto;
        $this->_cn = $cn;
        
        $this->_correoSup=$_correoSup;
        
        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo WHERE nombre='host'");

        $rsDatos = mysqli_fetch_assoc($datos);

        $this->_host = $rsDatos['valor'];

        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo WHERE nombre='puerto'");

        $rsDatos = mysqli_fetch_assoc($datos);

        $this->_puerto = $rsDatos['valor'];

        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo WHERE nombre='Username'");

        $rsDatos = mysqli_fetch_assoc($datos);

        $this->_userName = $rsDatos['valor'];

        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo WHERE nombre='Password'");

        $rsDatos = mysqli_fetch_assoc($datos);

        $this->_password = $rsDatos['valor'];

        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo WHERE nombre='SMTPSecure'");

        $rsDatos = mysqli_fetch_assoc($datos);

        $this->_SMTPSecure = $rsDatos['valor'];
        
        
        
    }

        
    public function sendMail(){
        
        try { 
        
            $mail = new PHPMailer(true);  

            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $this->_host;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $this->_userName;                 // SMTP username
            $mail->Password = $this->_password;                           // SMTP password
            $mail->SMTPSecure = $this->_SMTPSecure;                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $this->_puerto; 

            //Recipients
            $mail->setFrom($this->_userName,'Notifiacones');
            $mail->addAddress($this->_correoDestinatario, $this->_nombreDestinatario);     // Add a recipient
            //$mail->addReplyTo('noreply@.com', 'Notificaiones');

            //Adjuntos
            if ($this->_adjunto==true){
                $mail->addAttachment($this->_nombreAdjunto);
            }
            
            if($this->_correoSup != ""){
                $mail->addCC($this->_correoSup);
            }
            
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $this->_asunto;
            $mail->Body    = $this->_textoNotifiacion;
            $mail->AltBody = $this->_textoNotifiacion;

            $mail->send();
        
        } catch (Exception $e) {
             echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }      
        
    }
    
    public function setearValoresServidorCorreo(){
        
        $datos = mysqli_query($this->_cn, "SELECT valor FROM configcorreo");
        
        $rsDatos = mysqli_fetch_array($datos);
        
        $this->_host = $rsDatos[0][0];
        $this->_puerto = $rsDatos[0][1];
        $this->_userName = $rsDatos[0][2];
        $this->_password = $rsDatos[0][3];
        $this->_SMTPSecure = $rsDatos[0][4];
        
    }

    
    
    
}
