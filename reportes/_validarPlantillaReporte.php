<?php 
include('../conexion/conexion_admin.php');
require_once __DIR__ . '/plugins/mpdf/vendor/autoload.php';

include ('../reportes/enviarMail.class.php');

$_POST['id']=258;
$_POST['idReporte']=28;

$id = $_POST['id'];

$idReporte = $_POST['idReporte'];

$mpdf = new \Mpdf\Mpdf();

$pagina_inicio = file_get_contents('index.php');


$sqlDatosReporte = "SELECT re.id as Correlativo, cli.Nombre as NombreEmpresa, cli.Direccion, cli.Telefonos, cli.Sitioweb, cli.Codigo, cli.Nombre as NombreCliente, re.horas, re.millas, re.kilometros, re.ubicacionEquipo, re.nombreContacto, re.codDistribudor, re.ordenTrabajo,
ot.Modelo, ot.Serie, tr.Descripcion as TipoReporte, ot.FechaProgramada, t.Codigo, ot.Descripcion as intruccionesServicio, 
re.noPieza, re.descripcionPieza, re.cantidad, concat(s.Codigo,' - ',s.Descripcion) as codigoDesriptivo, re.numeroGrupoPieza,
re.descripcionGrupoPieza, case re.averia when 1 then 'Si' else 'No' end as averia, case re.componentePrincipal when 0 then 'No Aplica' else comp.Descripcion end as componentePrincipal,
ss.Descripcion as SubSistema,re.sintomasReportados, re.seleccionComponentesOperaciones, re.pruebasRealizadas, re.calibracionesAjustes,
re.mediciones, case re.analisisFluidos when 1 then 'Si' else 'No' end as analisisFluidos, cp.Descripcion as condicionesPiezas, re.descripcionComplementaria,
re.codigosFallas, re.diagnosticoFalla, re.observaciones, f.Foto as FirmaCliente, u.Firma as FirmaUsuario, cs.Descripcion as CondicionServicio, ce.Descripcion as CondicionEquipo,
concat(sup.Nombres, ' ' , sup.Apellidos) as NombreSupervisor, sup.Telefono as TelefonoSup, sup.Firma as FirmaSupervisor, re.observaciones
FROM reportesordentrabajo re
INNER JOIN orden_trabajo ot ON re.ordenTrabajo = ot.id 
INNER JOIN cliente cli ON ot.Cliente = cli.id 
INNER JOIN tipo_reporte tr ON re.idTipoRerpote = tr.Id
INNER JOIN tecnico t ON ot.Tecnico = t.id 
INNER JOIN sistema s ON re.codigoDesriptivo = s.Id  
INNER JOIN condicion_pieza cp ON re.condicionesPiezas = cp.Id
left JOIN componente_principal comp ON re.componentePrincipal = comp.Id 
INNER JOIN sub_sistema ss ON re.subSistema = ss.Id 
INNER JOIN fotos f ON re.ordenTrabajo = f.Orden and f.Tipo=4  
INNER JOIN usuario u ON t.Usuario = u.Id
INNER JOIN condicion_servicio cs ON re.condicionesServicio = cs.Id
INNER JOIN condicion_equipo ce ON re.condicionesServicio = ce.Id
INNER JOIN usuario sup ON ot.Supervisor = sup.Id
WHERE re.ordenTrabajo={$id}" ;


    $resultadoDatosReporte = (mysqli_query($cn,$sqlDatosReporte));
    
    $misDatos = mysqli_fetch_array($resultadoDatosReporte);
    
    //var_dump($misDatos);
    
    $pagina_inicio = str_replace('#nombreEmpresa', $misDatos['NombreCliente'], $pagina_inicio);
    $pagina_inicio = str_replace('#direccionCliente', $misDatos['Direccion'], $pagina_inicio);
    $pagina_inicio = str_replace('#telefonoCliente', $misDatos['Telefonos'], $pagina_inicio);
    $pagina_inicio = str_replace('#webRedes', $misDatos['Sitioweb'], $pagina_inicio);
    $pagina_inicio = str_replace('#nombreReporte',  $misDatos['TipoReporte'], $pagina_inicio);
    $pagina_inicio = str_replace('#codigoCliente', $misDatos['Codigo'], $pagina_inicio);
    $pagina_inicio = str_replace('#nombreCliente', $misDatos['NombreCliente'], $pagina_inicio);
    $pagina_inicio = str_replace('#modelo', $misDatos['Modelo'], $pagina_inicio);
    $pagina_inicio = str_replace('#numeroSerie', $misDatos['Serie'], $pagina_inicio);
    $pagina_inicio = str_replace('#hora', $misDatos['horas'], $pagina_inicio);
    $pagina_inicio = str_replace('#millas', $misDatos['millas'], $pagina_inicio);
    $pagina_inicio = str_replace('#kilometros', $misDatos['kilometros'], $pagina_inicio);    
    $pagina_inicio = str_replace('#ubicacionEquipo', $misDatos['ubicacionEquipo'], $pagina_inicio);
    $pagina_inicio = str_replace('#nombreContacto', $misDatos['nombreContacto'], $pagina_inicio);    
    
    $pagina_inicio = str_replace('#codDistribuidor', $misDatos['codDistribudor'], $pagina_inicio);    
    $pagina_inicio = str_replace('#ordenTrabajo', $misDatos['ordenTrabajo'], $pagina_inicio);    
    $pagina_inicio = str_replace('#ordenTrabajo', $misDatos['kilometros'], $pagina_inicio);
    $pagina_inicio = str_replace('#fechaServicio', $misDatos['FechaProgramada'], $pagina_inicio);    
    $pagina_inicio = str_replace('#codigoEmpleado', $misDatos['Codigo'], $pagina_inicio);
    $pagina_inicio = str_replace('#instruccionesServicio', $misDatos['intruccionesServicio'], $pagina_inicio);
    
    $pagina_inicio = str_replace('#numeroPiezaFallo', $misDatos['noPieza'], $pagina_inicio);
    $pagina_inicio = str_replace('#descripcionPieza', $misDatos['descripcionPieza'], $pagina_inicio);
    $pagina_inicio = str_replace('#cantidad', $misDatos['cantidad'], $pagina_inicio);
    $pagina_inicio = str_replace('#codigoDescriptivo', $misDatos['codigoDesriptivo'], $pagina_inicio);
    $pagina_inicio = str_replace('#numeroGrupoPieza', $misDatos['numeroGrupoPieza'], $pagina_inicio);
    $pagina_inicio = str_replace('#DescripcionGrupo', $misDatos['descripcionGrupoPieza'], $pagina_inicio);
    $pagina_inicio = str_replace('#averia', $misDatos['averia'], $pagina_inicio);
    
    $pagina_inicio = str_replace('#componentesPrincipales', $misDatos['componentePrincipal'], $pagina_inicio);    
    $pagina_inicio = str_replace('#subSistema', $misDatos['SubSistema'], $pagina_inicio);    
    $pagina_inicio = str_replace('#sintomaReportados', $misDatos['sintomasReportados'], $pagina_inicio);
    $pagina_inicio = str_replace('#seleccionComponentesOperaciones', $misDatos['seleccionComponentesOperaciones'], $pagina_inicio);
    $pagina_inicio = str_replace('#pruebasRealizadas', $misDatos['pruebasRealizadas'], $pagina_inicio);
    $pagina_inicio = str_replace('#calibracionesAjustesRealizados', $misDatos['calibracionesAjustes'], $pagina_inicio);
    $pagina_inicio = str_replace('#medicionesObtenidas', $misDatos['mediciones'], $pagina_inicio);
    $pagina_inicio = str_replace('#analisisFluidos', $misDatos['analisisFluidos'], $pagina_inicio);
    $pagina_inicio = str_replace('#condicionesPiezasComponentes', $misDatos['condicionesPiezas'], $pagina_inicio);
    $pagina_inicio = str_replace('#descripcioncomplementariaServicio', $misDatos['descripcionComplementaria'], $pagina_inicio);
    $pagina_inicio = str_replace('#codigosFallas', $misDatos['codigosFallas'], $pagina_inicio);
    $pagina_inicio = str_replace('#diagnosticoFalla', $misDatos['diagnosticoFalla'], $pagina_inicio);
    $pagina_inicio = str_replace('#observaciones', $misDatos['observaciones'], $pagina_inicio);
    
    
    $pagina_inicio = str_replace('#condicionServicio', $misDatos['CondicionServicio'], $pagina_inicio);
    $pagina_inicio = str_replace('#condicionEquipo', $misDatos['CondicionEquipo'], $pagina_inicio);
    $pagina_inicio = str_replace('#nombreC', $misDatos['NombreCliente'], $pagina_inicio);
    $pagina_inicio = str_replace('#correlativo', $misDatos['Correlativo'], $pagina_inicio);
    $pagina_inicio = str_replace('#nombreSupervisor', $misDatos['NombreSupervisor'], $pagina_inicio);
    $pagina_inicio = str_replace('#firmaSupervisor', $misDatos['FirmaSupervisor'], $pagina_inicio);
    $pagina_inicio = str_replace('#telefonoSupervisor', $misDatos['TelefonoSup'], $pagina_inicio);
    
    $pagina_inicio = str_replace('#imagenFirmaCliente', $misDatos['FirmaCliente'], $pagina_inicio);
    $pagina_inicio = str_replace('#imagenFirmaTecnico', $misDatos['FirmaUsuario'], $pagina_inicio);
    
    
echo $pagina_inicio;


$mpdf->WriteHTML($pagina_inicio);

$nombre_archivo = "OrdenDeTrabajo_{$id}.pdf";

$mpdf->Output($nombre_archivo, \Mpdf\Output\Destination::FILE);

$sql = "UPDATE reportesordentrabajo SET archivo='{$nombre_archivo}'  WHERE id={$idReporte} and ordenTrabajo={$id}" ;
    
    $resultado = (mysqli_query($cn,$sql));



    if($resultado){
        
        
        $obtenerCorreo="SELECT c.Id, c.Correo, c.Nombre , t.Nombre as NombreTec
        FROM cliente c
        INNER JOIN orden_trabajo ot ON c.id=ot.Cliente
        INNER JOIN tecnico t ON ot.Tecnico = t.Id
        WHERE ot.id={$id}";
        
        $resMail = mysqli_query($cn,$obtenerCorreo);       
        
        $res_Mail = mysqli_fetch_array($resMail);
        
        $textoMail = "Buen día {$res_Mail['Nombre']},
                        Nuestro técnico {$res_Mail['NombreTec']} ha llegado a sus instalaciones, agradeceremos su autorización para su ingreso.
                        Saludos cordiales. ";
        
        
        //verificar Datos
         //$mail = new enviarMail($cn, $res_Mail['Correo'], $res_Mail['Nombre'], "Notificacion: Orden de Trabajo finalizada", $textoMail ,true, $nombre_archivo);
//        
       //$resultadoMail = $mail->sendMail();
              
        
        $data['error']='false';
        $data['msg']='no hubo error';
        $data['file']=$nombre_archivo;
        //$data['neviomail']= $resultadoMail;
       
        echo json_encode($data);
    }else{
        $data['error']='true';
        $data['msg']='hubo error';

        echo json_encode($data);
    }

    mysqli_close($cn);