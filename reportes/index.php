<!DOCTYPE html>
<html lang="en">
  <head>
      
    <title>Reporte</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    
  </head>
  <body>
   <div class="container">
       <div style="background-color: black; height: 30px;">
           
       </div>
       <div style="background-color: darkorange; height: 15px;">
           
       </div>
     <div class="row">
        <div class="col col-lg-12" style="text-align: right;">
            <h1 style="padding: 0;margin-bottom: 0;"><span style="color: darkorange;">REPORTE</span> DE SERVICIO</h1>
            <h3 style="margin-top: 0;">COMPAÑIA GENERAL DE EQUIPOS</h3>
        </div>
     </div>
     <div class="row">
       <div class="col col-lg-6">
        <h1 class="encabezado">CLIENTE</h1>
        <table class="table table-condensed">
          <tr>
            <td>Nombre:</td>
            <td>#nombreEmpresa</td>            
          </tr>
          <tr>
            <td>Dirección de la empresa:</td>
            <td>#direccionCliente</td>            
          </tr>
          <tr>
            <td>Teléfonos:</td>
            <td>#telefonoCliente</td>
          </tr>
          <tr>            
            <td>Sitio web, redes sociales:</td>
            <td>#webRedes</td>
          </tr>
          <tr>
            <td>Nombre del reporte:</td>
            <td>#nombreReporte</td>
          </tr>
        </table>
       </div>
       <div class="col col-lg-6">
        <h1 class="encabezado">GENERALIDADES</h1>
        <table class="table table-condensed">
          <tr>
            <td>Código de cliente:</td>
            <td>#codigoCliente</td>            
          </tr>
          <tr>
            <td>Nombre del cliente:</td>
            <td>#nombreCliente</td>            
          </tr>
          <tr>
            <td>Modelo:</td>
            <td>#modelo</td>
          </tr>
          <tr>            
            <td>Número de serie:</td>
            <td>#numeroSerie</td>
          </tr>
          <tr>
            <td>Horas / Millas / Kilometros **:</td>
            <td>#hora / #millas / #kilometros</td>
          </tr>
          <tr>
            <td>Ubicación del equipo:</td>
            <td>#ubicacionEquipo</td>
          </tr>
          <tr>
            <td>Nombre de contacto:</td>
            <td>#nombreContacto</td>
          </tr>
        </table>
       </div>
     </div>
     <div class="row">
       <div class="col col-lg-12">
        <h1 class="encabezado">REFERENCIAS</h1>
        <table class="table table-condensed">
          <tr>
            <td>Código del distribuidor:</td>
            <td>#codDistribuidor</td>
          </tr>
          <tr>
            <td>Orden de trabajo:</td>
            <td>#ordenTrabajo</td>
          </tr>
          <tr>
            <td>Fecha del servicio:</td>
            <td>#fechaServicio</td>
          </tr>
          <tr>
            <td>Código del empleado:</td>
            <td>#codigoEmpleado</td>
          </tr>
          <tr>
            <td>Instrucciones del servicio:</td>
            <td>#instruccionesServicio</td>
          </tr>
        </table>
       </div>
     </div>
     <div class="row">
       <div class="col col-lg-12">
        <h1 class="encabezado">GARANTÍA</h1>
        <table class="table table-condensed">
          <tr>
            <td>Número de pieza que falló:</td>
            <td>#numeroPiezaFallo</td>
          </tr>
          <tr>
            <td>Descripción de la pieza:</td>
            <td>#descripcionPieza</td>
          </tr>
          <tr>
            <td>Cantidad:</td>
            <td>#cantidad</td>
          </tr>
          <tr>
            <td>Código descriptivo***:</td>
            <td>#codigoDescriptivo</td>
          </tr>
          <tr>
            <td>Número de grupo en que se halla la pieza:</td>
            <td>#numeroGrupoPieza</td>
          </tr>
          <tr>
            <td>Descripción del grupo:</td>
            <td>#DescripcionGrupo</td>
          </tr>
          <tr>
            <td>¿A causa de la avería quedó el producto inservible?</td>
            <td>#averia</td>
          </tr>
        </table>
       </div>
     </div>
     <div class="row">
       <div class="col col-lg-12">
         <h1 class="encabezado">SERVICIO</h1>
         <table class="table table-condensed">
           <tr>
             <td>Componentes principales:</td>
             <td>#componentesPrincipales</td>
           </tr>
           <tr>
             <td>Sub- sistemas:</td>
             <td>#subSistema</td>
           </tr>
           <tr>
             <td>Síntomas reportados</td>
             <td>#sintomaReportados</td>
           </tr>
           <tr>
             <td>Selección de componentes y operaciones:</td>
             <td>#seleccionComponentesOperaciones</td>
           </tr>
           <tr>
             <td>Pruebas realizadas:</td>
             <td>#pruebasRealizadas</td>
           </tr>
           <tr>
             <td>Calibraciones y ajustes realizados:</td>
             <td>#calibracionesAjustesRealizados</td>
           </tr>
           <tr>
             <td>Mediciones obtenidas:</td>
             <td>#medicionesObtenidas</td>
           </tr>
           <tr>
             <td>Análisis de fluidos:</td>
             <td>#analisisFluidos</td>
           </tr>
           <tr>
             <td>Condiciones de las piezas o componentes:</td>
             <td>#condicionesPiezasComponentes</td>
           </tr>
           <tr>
             <td>Descripción complementaria del servicio:</td>
             <td>#descripcioncomplementariaServicio</td>
           </tr>
           <tr>
             <td>Códigos de fallas:</td>
             <td>#codigosFallas</td>
           </tr>
           <tr>
             <td>Diagnóstico de la falla:</td>
             <td>#diagnosticoFalla</td>
           </tr>
           <tr>
             <td>Observaciones:</td>
             <td>#observaciones</td>
           </tr>
         </table>
       </div>
     </div>
     <div class="row ">
       <div class="col col-lg-12">
         <h1 class="encabezado">OTROS</h1>
         <table class="table table-condensed">
           <tr>
             <td>Condición del servicio:</td>
             <td>#condicionServicio</td>
           </tr>
           <tr>
             <td>Condición del equipo:</td>
             <td>#condicionEquipo</td>
           </tr>
           <tr>
             <td>Nombre del cliente:</td>
             <td>#nombreC</td>
           </tr>           
           <tr>
             <td>Nombre del supervisor:</td>
             <td>#nombreSupervisor</td>
           </tr>
           <tr>
             <td>Firma del supervisor:</td>
             <td>#firmaSupervisor</td>
           </tr>
           <tr>
             <td>Teléfono del supervisor:</td>
             <td>#telefonoSupervisor</td>
           </tr>
           <tr>
             <td>Número correlativo de reportes:</td>
             <td>#correlativo</td>
           </tr>
         </table>
       </div>
     </div>
       <div class="row">
           <div class="col col-lg-6 center-block center" style="height: 60px">
               <img src="../control/fotos/#imagenFirmaCliente" alt="firmaCliente" style="display: block;margin: 0 auto">
               <p style="text-align: center">Cliente</p>
           </div>
           <div class="col col-lg-6 center-block center" style="height: 60px">
               <img src="#imagenFirmaTecnico" alt="firmaTecnico" style="display: block;margin: 0 auto">
               <p style="text-align: center">Técnico</p>
           </div>
       </div>
    
   </div>

    
  </body>
</html>