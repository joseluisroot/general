<?php
include('../conexion/conexion_usuario.php');

$rs = mysqli_query($cn, "SELECT orden_trabajo.Id,
       orden_trabajo.Codigo,
       cliente.Codigo as ClienteCodigo ,
       cliente.Nombre as ClienteNombre,
       marca.Codigo as MarcaCodigo,
       marca.Nombre as MarcaNombre,
       orden_trabajo.FechaApertura,
       orden_trabajo.Descripcion,
       orden_trabajo.Serie,
       orden_trabajo.Modelo,
       tecnico.Codigo as TecnicoCodigo,
       tecnico.Nombre as TecnicoNombre,
       date_format(orden_trabajo.FechaProgramada, '%d/%m/%Y') as FechaProgramada,
       date_format(orden_trabajo.FechaApertura, '%d/%m/%Y') as FechaApertura,
       orden_trabajo.Estado       
  FROM ((orden_trabajo orden_trabajo
         INNER JOIN tecnico tecnico
            ON (orden_trabajo.Tecnico = tecnico.Id))
        INNER JOIN cliente cliente
           ON (orden_trabajo.Cliente = cliente.Id))
       INNER JOIN marca marca ON (orden_trabajo.Marca = marca.Id)
 WHERE (orden_trabajo.Id = " . $_GET["Id"] . ")");
$row = mysqli_fetch_array($rs);


$rsMotvos = mysqli_query($cn, "SELECT idMotivo, nombre FROM motivos");

$rowMotivos = mysqli_fetch_array($rsMotvos);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="js/jquery-1.11.2.min.js"></script>


<!-- geolocalizacion-->
<script language="JavaScript">



</script>
<!-- fin geolocalizacion-->

</head>
<body onload="geoInmuebleFindMe()" class="hold-transition skin-blue sidebar-mini" >
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="col-md-6 col-xs-12">
                    <div class="x_title">
                        <h2>Orden de trabajo <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <p><strong>ORDEN : </strong><span id="noOrden"><?php echo $row[1] . '  '; ?></span> <strong>FECHA DE APERTURA:</strong><?php echo ' ' . $row[13]; ?>         </p>
                            <p><strong> CLIENTE : </strong><?php echo $row[2] . ' - ' . $row[3]; ?> </p>
                            <p><strong> DESCRIPCION : </strong><?php echo $row[7] . '<strong> MARCA:</strong> ' . $row[4] . ' - ' . $row[5]; ?> </p>
                            <p><strong> SERIE : </strong><?php echo $row[8] . ' <strong>MODELO:</strong> ' . $row[9]; ?> </p>
                            <p><strong> TECNICO ASIGNADO : </strong><?php echo $row[10] . ' - ' . $row[11]; ?> </p>
                            <p><strong> FECHA PROGRAMADA : </strong><?php echo $row[12]; ?> </p>



                            <?php
                            if (isset($_POST["Iniciar"])) {
                                if ($row[14] == 2) {

                                    echo ' <div align="center" class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<strong> Orden de trabajo iniciada correctamente.</strong></div>';
                                } else {
                                    echo ' <div align="center" class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Error, la orden ya ha sido iniciada.</strong></div>';
                                }
                            }
                            ?>

                        </form>
                        <table align="center">
                            <tr><div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="txt_latitud" readonly>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control" id="txt_longitud" readonly>
                            </div>
                            <input type="hidden" id="hdNoOrden" value="<?= $_GET['Id'] ?>"></tr>
                            <tr align="center">
                                <td colspan="2"> &nbsp;&nbsp; <strong>Horas</strong> &nbsp;&nbsp;</td>
                                <td colspan="2">&nbsp;&nbsp;<strong>Minutos</strong>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;<strong>Segundos</strong>&nbsp;&nbsp;</td>                           
                            </tr>
                            <tr align="center">
                                <td> <div id="hour">00</div></td>
                                <td><div class="divider">:</div></td>
                                <td><div id="minute">00</div></td>
                                <td> <div class="divider">:</div></td>
                                <td><div id="second">00</div>  </td>
                            </tr>

                            <tr align="center">
                                <td colspan="5">
                                    <button id="btn-comenzar" class="btn btn-success">Comenzar</button>
                                    <button id="btn-detener" class="btn btn-success">Llegada al lugar</button>
                                    <button id="btn-iniciar" class="btn btn-primary">&nbsp;&nbsp;&nbsp;Iniciar Servicio&nbsp;&nbsp;&nbsp;</button>
                                    <button id="btn-cerrar" class="btn btn-danger">&nbsp;&nbsp;&nbsp;Cerrar Orden&nbsp;&nbsp;&nbsp;</button>
                                    <button id="btn-receso" class="btn btn-warning"><i class="fa fa-coffee"></i>&nbsp;Iniciar Receso</button>
                                    <button id="btn-fin-receso" class="btn btn-danger"><i class="fa fa-coffee"></i>&nbsp;Finalizar Receso</button>
                                </td>                            
                            </tr>

                            <tr align="center" id="trReportes">
                                <td colspan="5">
                                    <h4>Reportes </h4>
                                    <a href="firma.php?Id=<?php echo $row[0]; ?>"><button  class="btn btn-primary"><i class="fa fa-book"></i>&nbsp;Firma</button></a>
                                    <a href="reporte_servicio.php?Id=<?php echo $row[0]; ?>"><button  class="btn btn-primary"><i class="fa fa-book"></i>&nbsp;Servicio</button></a>
                                    <a href="reporte/reporte_notas.php?Id=<?php echo $row[0]; ?>"><button class="btn btn-primary"><i class="fa fa-book"></i>&nbsp;Notas</button></a>
                                    <a href="reporte_servicio.php?Id=<?php echo $row[0]; ?>"><button  class="btn btn-primary"><i class="fa fa-book"></i>&nbsp;PSSRs</button></a>
                                </td>                            
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="clearfix"></div>

            </div>

        </div>

        <!-- Modal Cierre -->
        <div class="modal fade" id="modalCierre" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cerrar Orden de Trabajo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" id="formCerrarOrden" method="POST" data-parsley-validate class="form-horizontal form-label-left" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="Motivo" class="col-sm-2 control-label">Motivo</label>
                                    <div class="col-sm-10">
                                        <select id="Motivo" name="Motivo" required class="form-control">
                                            <option value="0">Seleccionar...</option>
                                            <?php
                                            foreach ($rsMotvos as $value) {
                                                echo '<option value="' . $value['idMotivo'] . '">' . $value['nombre'] . '</option>';
                                            }
                                            ?>
                                        </select>
            <!--                            <input type="text" class="form-control" id="Motivo" name="Motivo" autofocus="true" onBlur="this.value = this.value.toUpperCase();" required="true" >-->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Observacion" class="col-sm-2 control-label">Observaciones</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="Observacion" name="Observacion" onBlur="this.value = this.value.toUpperCase();" rows="5" ></textarea>
                                    </div>
                                </div>
                            </div>                    
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnCerrarOrdenTrabajo">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal receso -->
        <div class="modal fade" id="modalReceso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-tipo="1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Receso en Orden de Trabajo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" id="formReceso" method="POST" data-parsley-validate class="form-horizontal form-label-left" >
                            <input id="idReceso" value="" type="hidden">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="Observacion" class="col-sm-2 control-label">Observación</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="ObservacionReceso" name="ObservacionReceso" onBlur="this.value = this.value.toUpperCase();" rows="5" ></textarea>
                                    </div>
                                </div>
                            </div>                    
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnGuardarReceso">Guardar</button>
                    </div>
                </div>
            </div>
        </div>


        <script>

            var puntos = {
                pLatitude: "",
                pLongitud: ""
            };

            $(document).ready(function () {



                verificarEstado();

                var tiempo = {
                    hora: 0,
                    minuto: 0,
                    segundo: 0
                };

                var tiempo_corriendo = null;

                $("#btn-comenzar").click(function () {


                    if (!confirm('Esta seguro que desea iniciar traslado de la orden de trabajo?')) {
                        return false;
                        //alert('hola');
                    }


                    tiempo_corriendo = setInterval(function () {
                        // Segundos
                        tiempo.segundo++;
                        if (tiempo.segundo >= 60)
                        {
                            tiempo.segundo = 0;
                            tiempo.minuto++;
                        }

                        // Minutos
                        if (tiempo.minuto >= 60)
                        {
                            tiempo.minuto = 0;
                            tiempo.hora++;
                        }

                        $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
                        $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                        $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);
                    }, 1000);

                    $.post("reportarRutaOrdenTrabajo.php", {tipo: 1, noOrden: $('#noOrden').text().trim(), position: $('#txt_latitud').val() + ',' + $('#txt_longitud').val()})
                            .done(function (data) {

                                var res = $.parseJSON(data);

                                if (res.error == 'false') {
                                    $('#btn-receso').show();
                                    $('#btn-comenzar').hide();
                                    $('#btn-detener').show();
                                    alert('Inicio de traslado iniciado');
                                } else {
                                    alert('Ocurrio un error al iniciar la orden de trabajo');
                                }

                            });

                });

                $('#btn-iniciar').on('click', function () {
                    if (!confirm('Esta seguro que desea iniciar el servicio?')) {
                        return false;
                        //alert('hola');
                    }

                    puntos = getPuntoGeo();

                    $('#txt_latitud').val(puntos.pLatitude);
                    $('#txt_longitud').val(puntos.pLongitud);

                    $.post("reportarOrdenTrabajo.php", {tipo: 1, noOrden: $('#noOrden').text().trim(), position: puntos.pLatitude + ',' + puntos.pLongitud})
                            .done(function (data) {

                                var res = $.parseJSON(data);

                                if (res.error == 'false') {
                                    $('#btn-iniciar').hide();
                                    $('#btn-cerrar').show();
                                    alert('Orden de trabajo iniciada');
                                } else {
                                    alert('Ocurrio un error al iniciar la orden de servicio');
                                }



                            });



                });

                $('#btn-detener').on('click', function () {
                    if (!confirm('Esta seguro que desea reportar la llegada al lugar?')) {
                        return false;
                        //alert('hola');
                    }

                    puntos = getPuntoGeo();

                    $.post("reportarRutaOrdenTrabajo.php", {tipo: 2, noOrden: $('#noOrden').text().trim(), position: $('#txt_latitud').val() + ',' + $('#txt_longitud').val()})
                            .done(function (data) {

                                var res = $.parseJSON(data);

                                if (res.error == 'false') {
                                    $('#btn-detener').hide();
                                    $('#btn-iniciar').show();
                                    alert('Llegada al lugar reportada');
                                } else {
                                    alert('Ocurrio un error al reportar la llegada al lugar');
                                }

                            });
                    clearInterval(tiempo_corriendo);
                });

                $("#btn-cerrar").on('click', function () {
                    $('#modalCierre').modal('show');
                });

                $("#btn-receso").on('click', function () {
                    $('#modalReceso').modal('show');
                });

                $('#btnGuardarReceso').on('click', function (e) {
                    e.preventDefault();

                    puntos = getPuntoGeo();

                    var receso = {
                        Orden: $('#hdNoOrden').val(),
                        Observacion: $('#ObservacionReceso').val(),
                        GeoInicio: $('#txt_latitud').val() + ',' + $('#txt_longitud').val(),
                        tipo: $('#modalReceso')[0].attributes[6].nodeValue
                    };

                    var msg;
                    if ($('#modalReceso')[0].attributes[6].nodeValue == "2") {
                        msg = 'Esta seguro que desea finalizar el receso?';
                    } else {
                        msg = 'Esta seguro que desea inicar un receso?';
                    }

                    if (!confirm(msg)) {
                        return false;
                    }

                    $.ajax({
                        url: 'recesoOrden.php',
                        type: 'POST',
                        data: receso,
                        success: function (data) {
                            var res = $.parseJSON(data);

                            if (res.error == 'false') {

                                if ($('#modalReceso')[0].attributes[6].nodeValue == "2") {
                                    $('#btn-receso').show();
                                    $('#btn-fin-receso').hide();
                                    $('#modalReceso').modal('hide');
                                    $('#modalReceso').attr('data-tipo', 1);
                                    $('#ObservacionReceso').val('');

                                    //dhabilitando la finalizacion de la orden de trabajo
                                    $('#btn-cerrar').show();

                                    alert('Receso finalizado');

                                } else {
                                    $('#btn-receso').hide();
                                    $('#btn-fin-receso').show();

                                    //deshabilitando la finalizacion de la orden de trabajo
                                    $('#btn-cerrar').hide();

                                    $('#modalReceso').modal('hide');
                                    alert('Receso Reportado');
                                }


                            } else {

                                if ($('#modalReceso')[0].attributes[6].nodeValue == "2") {
                                    alert('Ocurrio un error al finalizar el receso');
                                } else {
                                    alert('Ocurrio un error al reportar el receso');
                                }

                            }
                        }
                    });

                });

                $('#btnCerrarOrdenTrabajo').on('click', function (e) {


                    if (!confirm('Esta seguro que desea cerrar la orden de trabajo?')) {
                        return false;
                    }

                    var cierre = {
                        Orden: $('#hdNoOrden').val(),
                        Motivo: $('#Motivo').val(),
                        Observacion: $('#Observacion').val(),
                        position: $('#txt_latitud').val() + ',' + $('#txt_longitud').val()
                    };


                    $.ajax({
                        url: 'cerrarOrdenTrabajo.php',
                        type: 'POST',
                        data: cierre,
                        success: function (data) {
                            var res = $.parseJSON(data);
                            if (res.error == 'false') {
                                $('#btn-receso').hide();
                                $('#btn-cerrar').hide();
                                $('#trReportes').show();
                                $('#modalCierre').modal('hide');

                                alert('Orden de trabajo Cerrada');
                            } else {
                                alert('Ocurrio un error al cerrar la orden de trabajo');
                            }
                        },
                        error: function (result) {
                            alert('ERROR ' + result.status + ' ' + result.statusText);
                        }
                    });
//                    $.post("cerrarOrdenTrabajo.php", cierre)
//                            .done(function (data) {
//
//                                var res = $.parseJSON(data);
//
//                                if (res.error == 'false') {
//                                    $('#btn-receso').hide();
//                                    $('#btn-cerrar').hide();
//                                    $('#trReportes').show();
//                                    $('#modalCierre').modal('hide');
//
//                                    alert('Orden de trabajo Cerrada');
//                                } else {
//                                    alert('Ocurrio un error al cerrar la orden de trabajo');
//                                }
//                            })
//                            .fail(function( jqXHR, textStatus, errorThrown ){
//                                debugger;
//                                console.log(jqXHR);
//                            });


                });

                $('#btn-fin-receso').on('click', function () {

                    //$('#modalReceso').attr('data-tipo',0);                

                    $.post("recesoOrden.php", {id: $('#hdNoOrden').val(), tipo: 0})
                            .done(function (data) {
                                var res = $.parseJSON(data);

                                if (res.error == "false") {
                                    $('#modalReceso').modal('show');
                                    $('#ObservacionReceso').val(res.data.Observacion);
                                    $('#idReceso').val(res.data.Id);
                                    $('#modalReceso').attr('data-tipo', 2);

                                }

                            });
                });

                function verificarEstado() {

                    $.get("datosOrdenTrabajo.php", {id: $('#hdNoOrden').val()})
                            .done(function (data) {

                                $('#trReportes').hide();

                                var res = $.parseJSON(data);

                                console.log(res);

                                if (res.error == false) {
                                    //debugger;

                                    if (res.data.estado == '2') {

                                        $('#btn-cerrar').hide();
                                        $('#btn-receso').hide();
                                        $('#btn-fin-receso').hide();
                                        $('#btn-detener').hide();
                                        $('#btn-iniciar').hide();
                                    }

                                    if (res.data.estado == '3') {

                                        $('#btn-cerrar').show();
                                        $('#btn-receso').show();
                                        $('#btn-comenzar').hide();
                                        $('#btn-detener').show();
                                        $('#btn-iniciar').hide();
                                    }

                                    if (res.data.estado == '4') {

                                        $('#btn-receso').show();
                                        $('#btn-comenzar').hide();
                                        $('#btn-detener').hide();
                                        $('#btn-iniciar').show();
                                    }

                                    if (res.data.estado == '5') {

                                        $('#btn-cerrar').show();
                                        $('#btn-receso').show();
                                        $('#btn-comenzar').hide();
                                        $('#btn-iniciar').hide();
                                        $('#btn-detener').hide();
                                        $('#trReportes').hide();
                                    }

                                    if (res.data.estado == '6') {

                                        $('#btn-cerrar').hide();
                                        $('#btn-receso').hide();
                                        $('#btn-comenzar').hide();
                                        $('#btn-iniciar').hide();
                                        $('#btn-detener').hide();
                                        $('#trReportes').show();
                                    }

                                    if (res.receso == false) {
                                        $('#btn-fin-receso').hide();

//                                        if (res.data.estado != '2'){
//                                            $('#btn-receso').hide();
//                                        }


                                    } else {
                                        $('#btn-fin-receso').show();
                                        $('#btn-receso').hide();
                                        $('#btn-cerrar').hide();
                                    }

                                }


                            });

                }

            });

            function geoInmuebleFindMe() {

                var geo_options = {
                    enableHighAccuracy: true,
                    maximumAge: 30000,
                    timeout: 5000
                }

                function success(position) {
                    var latitude = position.coords.latitude;
                    var longitude = position.coords.longitude;
                    var altitude = position.coords.altitude;
                    var accuracy = position.coords.accuracy;

                    //do something with above position thing e.g. below
                    //alert('I am here! lat:' + latitude +' and long : ' +longitude );
                    //
                    //AQUI PODES VER QUE LE ASIGNO LOS VALORES DE LATITUD Y LONGITUD
                    document.getElementById("txt_latitud").value = latitude;
                    document.getElementById("txt_longitud").value = longitude;


                    //document.getElementById("altitud").value=altitude;
                    //document.getElementById("exactitud").value=accuracy;
                }

                function error(error) {
                    console.log("Unable to retrieve your location due to " + error.code + " : " + error.message);
                }
                ;



                navigator.geolocation.getCurrentPosition(success, error, geo_options);
            }

            function getPuntoGeo() {

                var geo_options = {
                    enableHighAccuracy: true,
                    maximumAge: 30000,
                    timeout: 5000
                }

                function success(position) {
                    var latitude = position.coords.latitude;
                    var longitude = position.coords.longitude;
                    var altitude = position.coords.altitude;
                    var accuracy = position.coords.accuracy;

                    //do something with above position thing e.g. below
                    //alert('I am here! lat:' + latitude +' and long : ' +longitude );
                    //
                    //AQUI PODES VER QUE LE ASIGNO LOS VALORES DE LATITUD Y LONGITUD
                    puntos.pLatitude = latitude;
                    puntos.pLongitud = longitude;
                    $('#txt_latitud').val(latitude);
                    $('#txt_longitud').val(longitude);


                    //document.getElementById("altitud").value=altitude;
                    //document.getElementById("exactitud").value=accuracy;
                }

                function error(error) {
                    console.log("Unable to retrieve your location due to " + error.code + " : " + error.message);
                }
                ;

                navigator.geolocation.getCurrentPosition(success, error, geo_options);

                return puntos;
            }
        </script>



        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>



</body>
</html>
