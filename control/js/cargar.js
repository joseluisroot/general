$(document).ready(function(){

    $('.btn_enviar').on("click", function(evt){
        
        evt.preventDefault();
        
        var tipo = $(this).data('tipo');
        
        //alert(tipo);
        
        // declaro la variable formData e instancio el objeto nativo de javascript new FormData
        var formData;
        
        //formData = new FormData(document.getElementById("frmPicDiagnostico"));
                
        if (tipo==1){            
            //formData = new FormData(document.getElementById("frmPicDiagnostico"));
            formData = new FormData($('#frmPicDiagnostico').get(0)); 
        }else if(tipo==2){
            //formData = new FormData(document.getElementById("frmPicCondiciones"));
            formData = new FormData($('#frmPicCondiciones').get(0)); 
        }else if(tipo==3){
            //formData = new FormData(document.getElementById("frmPicComplemento"));
            formData = new FormData($('#frmPicComplemento').get(0)); 
        }else if(tipo==4){
            //formData = new FormData(document.getElementById("frmPicComplemento"));
            formData = new FormData($('#frmPicDiagnostico4').get(0)); 
        }else if(tipo==5){
            //formData = new FormData(document.getElementById("frmPicComplemento"));
            formData = new FormData($('#frmPicCondiciones5').get(0)); 
        }else if(tipo==6){
            //formData = new FormData(document.getElementById("frmPicComplemento"));
            formData = new FormData($('#frmPicComplemento6').get(0)); 
        }
        
         var variablesData = {
                formImg:formData
            }

        // iniciar el ajax
        $.ajax({
            url: 'procesar-subida.php',
            // el metodo para enviar los datos es POST
            type: "POST",
            // colocamos la variable formData para el envio de la imagen
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() 
            {
                $('#mensaje'+tipo).prepend('<img src="facebook.gif" width="50" />');
            },
            success: function(data)
            {
                //debugger;
                
                if (tipo==1){
                    $('#foto1').val("");
                }else if(tipo==2){
                    $('#foto2').val("");
                }else if(tipo==3){
                    $('#foto3').val("");
                }else if(tipo==4){
                    $('#foto4').val("");
                }else if(tipo==5){
                    $('#foto5').val("");
                }else if(tipo==6){
                    $('#foto6').val("");
                }
                
                $('#mensaje'+tipo).fadeOut("fast",function(){
                    $('#mensaje'+tipo).html(data);
                });
                $('#mensaje'+tipo).fadeIn("slow");
            } 
        });


    });

});