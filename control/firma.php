<?php
include('../conexion/conexion_usuario.php');
$rs = mysqli_query($cn, "select * from fotos where Orden = " . $_GET["Id"] . " and Tipo = 4");
$row_count = mysqli_num_rows($rs);
?>
<html>
    <head>
        <title>Sketch Pad</title>
        <style type="text/css">

            #content{
                background-color:#ddd;
                border: solid;
                width: 300px;
                height: 150px ;
            }

            #canvas{
                cursor:crosshair ;
                background-color:#fff;
            }

            .style1
            {
                width: 100%;
            }


        </style>
        <script type="text/javascript">

            var ctx, color = "#000";

            document.addEventListener("DOMContentLoaded", function () {
                // setup a new canvas for drawing wait for device init
                setTimeout(function () {
                    newCanvas();
                }, 1000);
            }, false);



            // function to setup a new canvas for drawing
            function newCanvas() {
                //define and resize canvas

                var canvas = '<canvas id="canvas" width="300px" height="150px"></canvas>';
                document.getElementById("content").innerHTML = canvas;

                // setup canvas
                ctx = document.getElementById("canvas").getContext("2d");
                ctx.strokeStyle = color;
                ctx.lineWidth = 8;

                // setup to trigger drawing on mouse or touch
                drawTouch();
                //   drawPointer();
                drawMouse();


            }

            function selectColor(el) {


                ctx.beginPath();
                ctx.strokeStyle = "#000";
            }



            // prototype to	start drawing on touch using canvas moveTo and lineTo
            var drawTouch = function () {
                var start = function (e) {
                    ctx.beginPath();
                    var divcanvas = document.getElementById("canvas");
                    x = e.changedTouches[0].pageX - getDimensions(divcanvas).x;
                    y = e.changedTouches[0].pageY - getDimensions(divcanvas).y;
                    ctx.moveTo(x, y);
                };
                var move = function (e) {
                    e.preventDefault();
                    var divcanvas = document.getElementById("canvas");
                    x = e.changedTouches[0].pageX - getDimensions(divcanvas).x;
                    y = e.changedTouches[0].pageY - getDimensions(divcanvas).y;
                    ctx.lineTo(x, y);
                    ctx.stroke();
                };
                document.getElementById("canvas").addEventListener("touchstart", start, false);
                document.getElementById("canvas").addEventListener("touchmove", move, false);
            };





            // prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
            var drawPointer = function () {
                var start = function (e) {
                    e = e.originalEvent;
                    ctx.beginPath();
                    x = e.pageX;
                    y = e.pageY;
                    ctx.moveTo(x, y);
                };
                var move = function (e) {
                    e.preventDefault();
                    e = e.originalEvent;
                    x = e.pageX;
                    y = e.pageY;
                    ctx.lineTo(x, y);
                    ctx.stroke();
                };
                document.getElementById("canvas").addEventListener("MSPointerDown", start, false);
                document.getElementById("canvas").addEventListener("MSPointerMove", move, false);
            };


            // prototype to	start drawing on mouse using canvas moveTo and lineTo
            var drawMouse = function () {
                var clicked = 0;
                var start = function (e) {
                    clicked = 1;
                    ctx.beginPath();


                    var divcanvas = document.getElementById("canvas");

                    x = e.pageX - getDimensions(divcanvas).x;
                    y = e.pageY - getDimensions(divcanvas).y;
                    ctx.moveTo(x, y);



                };
                var move = function (e) {
                    if (clicked) {

                        var divcanvas = document.getElementById("content");

                        x = e.pageX - getDimensions(divcanvas).x;
                        y = e.pageY - getDimensions(divcanvas).y;
                        ctx.lineTo(x, y);
                        ctx.stroke();

                    }
                };
                var stop = function (e) {
                    clicked = 0;
                };
                document.getElementById("canvas").addEventListener("mousedown", start, false);
                document.getElementById("canvas").addEventListener("mousemove", move, false);
                document.addEventListener("mouseup", stop, false);
            };







            getDimensions = function (oElement) {
                var x, y, w, h;
                x = y = w = h = 0;
                if (document.getBoxObjectFor) { // Mozilla
                    var oBox = document.getBoxObjectFor(oElement);
                    x = oBox.x - 1;
                    w = oBox.width;
                    y = oBox.y - 1;
                    h = oBox.height;
                } else if (oElement.getBoundingClientRect) { // IE
                    var oRect = oElement.getBoundingClientRect();
                    x = oRect.left - 2;
                    w = oElement.clientWidth;
                    y = oRect.top - 2;
                    h = oElement.clientHeight;
                }
                return {x: x, y: y, w: w, h: h};
            }




        </script>

        <script src="js/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {

                verificarEstado();

                // Send the drawn image to the server
                $('#sendBtn').live('click', function () {
                    var Pic = document.getElementById("canvas").toDataURL("image/jpg");
                    //Pic = Pic.replace(/^data:image\/(png|jpg);base64,/, "")

                    console.log(Pic);

                    var data = {
                        imageData: Pic,
                        orden: $('#orden').val()
                    }
                    //alert(data);
                    // Sending the image data to Server
                    $.ajax({
                        type: 'POST',
                        url: 'saveFirma.php',
                        data: data,
                        //contentType: 'application/json; charset=utf-8',
                        //dataType: 'json',

                        success: function (data) {
                            var res = $.parseJSON(data);


                            if (res.error == 'false') {
                                alert('Firma guardada');
                            } else {
                                alert('Ocurrio un error al guardar firma');
                            }
                        }
                        
                    });
                    
                    document.getElementById('Estado').value = '1';
                        verificarEstado();
                });



                function verificarEstado()
                {
                    if (document.getElementById('Estado').value > '0')
                    {
                        $("#sendBtn").prop('disabled', true);
                    }
                }


            });

        </script>



    </head>
    <body>
        <div id="page">
            <input type="hidden" value="<?php echo $_GET['Id'] ?>" id="orden">
            <input type="hidden" value="<?php echo $row_count; ?>" id="Estado">
            <table class="style1">
                <tr>
                    <td width="7%">&nbsp;
                    </td>
                    <td width="86%" style="text-align: center">&nbsp;


                    </td>
                    <td width="7%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="7%">&nbsp;
                    </td>
                    <td width="86%" style="text-align: center">


                        <div id="content"><p style="text-align:center">Cargando firma...</p></div>


                    </td>
                    <td width="7%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="7%">&nbsp;
                    </td>
                    <td width="86%" >
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="Button1" onclick="newCanvas()" type="button" value="Limpiar" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="sendBtn" type="button" value="Guardar" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-info" href="orden_detalle.php?Id=<?php echo $_GET["Id"]; ?>"><input type="button" value="Resgresar" /></a>

                    </td>
                    <td width="7%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="7%">


                    </td>
                    <td width="86%">&nbsp;</td>
                    <td width="7%">&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>