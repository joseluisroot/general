<?php
if (isset($_GET["Id"])) {
    include('../conexion/conexion_usuario.php');

    $rs = mysqli_query($cn, "SELECT orden.Id,
       orden.Codigo AS CodOrden,
       cliente.Codigo AS CodCliente,
       cliente.Nombre AS Cliente,
       tecnico.Codigo AS CodTecnico,
       tecnico.Nombre AS Tecnico,
       marca.Codigo AS CodMarca,
       marca.Nombre AS Marca,
       orden.FechaApertura,
       orden.Descripcion,
       orden.Serie,
       orden.Modelo,
       estado.Descripcion AS Estado,
       estado.Id AS IdEstado,
       orden.FechaProgramada,
       tecnico.Id AS IdTecnico,       
       orden.Nivel,
       orden.Supervisor
  FROM (((orden_trabajo orden
          INNER JOIN marca marca ON (orden.Marca = marca.Id))
         INNER JOIN cliente cliente ON (orden.Cliente = cliente.Id))
        LEFT JOIN tecnico tecnico ON (orden.Tecnico = tecnico.Id))
       INNER JOIN estado estado ON (orden.Estado = estado.Id) where orden.Id = " . $_GET["Id"]);

    $row = mysqli_fetch_array($rs);
    ?>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="js/jquery-1.11.2.min.js"></script>


    </head>
    <body>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br />

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-align-left"></i> Orden de trabajo<small>Orden</small></h2>                                  
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start accordion -->
                                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <h4 class="panel-title">Asignacion de orden de trabajo</h4>
                                            </a>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">


                                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">CODIGO ORDEN</label>
                                                            <div class="col-sm-10">
                                                                <input type="hidden" id="Rol" name="Rol"  value="<?php echo $_SESSION["Rol"] ?>" class="form-control" >
                                                                <input type="hidden" id="Id" name="Id" class="form-control" value="<?php echo $row[0]; ?>" >
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[1]; ?>" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">FECHA APERTURA</label>
                                                            <div class="col-sm-10">
                                                                <input type="date" readonly="true" name="FechaApertura" class="form-control" value="<?php echo $row[8]; ?>"  >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">CLIENTE</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[2] . ' - ' . $row[3]; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">MARCA</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[6] . ' - ' . $row[7]; ?>"  >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">DESCRIPCION</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[9]; ?>"  >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">SERIE</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[10]; ?>" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">MODELO</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" readonly="true" name="Codigo" class="form-control" value="<?php echo $row[11]; ?>" >
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">SUPERVISOR</label>
                                                            <div class="col-sm-10">

                                                                <select name="Supervisor" id="Supervisor" class="form-control">
                                                                    <?php
                                                                    $rs2 = mysqli_query($cn, "SELECT usuario.Id,usuario.Usuario, usuario.Nombres, usuario.Apellidos FROM usuario usuario where Rol = 2");
                                                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                                                        ?>
                                                                        <option value="<?php echo $row2[0]; ?>"
                                                                        <?php
                                                                        if ($row["Supervisor"] == $row2[0]) {
                                                                            echo 'selected';
                                                                        }
                                                                        ?>  
                                                                                ><?php echo $row2[2] . ' ' . $row2[3]; ?></option> 
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                </select>


                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">FECHA PROGRAMADA</label>
                                                            <div class="col-sm-10">
                                                                <input type="date"  id="FechaProgramada" name="FechaProgramada"  value="<?php echo $row[14]; ?>" class="form-control" >
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">TECNICO</label>
                                                            <div class="col-sm-10">

                                                                <select name="Tecnico" id="Tecnico" class="form-control">
                                                                    <?php
                                                                    $rs2 = mysqli_query($cn, "SELECT tecnico.Id, tecnico.Codigo, tecnico.Nombre,tecnico.PrimerApellido,tecnico.SegundoApellido FROM tecnico tecnico order by tecnico.Nombre");
                                                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                                                        ?>
                                                                        <option value="<?php echo $row2[0]; ?>"
                                                                        <?php
                                                                        if ($row[15] == $row2[0]) {
                                                                            echo 'selected';
                                                                        }
                                                                        ?>  
                                                                                ><?php echo $row2[2] . $row2[3] . ' ' . $row2[4] . ' ' . ' - ' . $row2[1]; ?></option> 
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                </select>


                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">ESTADO</label>
                                                            <div class="col-sm-10">
                                                                <select name="Estado" id="Estado" name="Estado" class="form-control">
                                                                    <?php
                                                                    $rs2 = mysqli_query($cn, "SELECT * from estado");
                                                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                                                        ?>
                                                                        <option value="<?php echo $row2[0]; ?>"
                                                                        <?php
                                                                        if ($row[13] == $row2[0]) {
                                                                            echo 'selected';
                                                                        }

                                                                        if ($_SESSION["Rol"] != 1 && $row2[0] > 2 && $row2[0] < 10) {
                                                                            echo 'disabled';
                                                                        }
                                                                        ?>  
                                                                                ><?php echo $row2[1]; ?></option> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail2" class="col-sm-2 control-label">PRIORIDAD</label>
                                                            <div class="col-sm-10">
                                                                <select name="Nivel" id="Nivel" name="Nivel" class="form-control">
    <?php
    $rs2 = mysqli_query($cn, "SELECT * from nivel_orden");
    while ($row2 = mysqli_fetch_array($rs2)) {
        ?>
                                                                        <option value="<?php echo $row2[0]; ?>"
                                                                        <?php
                                                                        if ($row['Nivel'] == $row2[0]) {
                                                                            echo 'selected';
                                                                        }
                                                                        ?>  
                                                                                ><?php echo $row2[1]; ?></option> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-group" align="center">
                                                            <input type="button" id="btn_modificar" name="btn_modificar"  value="Modificar" class="btn btn-success" >&nbsp;&nbsp;&nbsp;
                                                            <a href="orden_trabajo.php"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                                                        </div>


                                                        <div class="box-footer" id="targetDiv">



                                                        </div>




                                                    </form>



                                                </div>
                                            </div>
                                        </div>




                                        <!--Actividades-->
                                        <div class="panel">
                                            <a class="panel-heading collapsed" class="panel-collapse collapse in" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <h4 class="panel-title">Actividades</h4>
                                            </a>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">

    <?php
    $rs = mysqli_query($cn, "Select orden.Id,
                                                    orden.Codigo,
                                                    orden.FechaApertura,
                                                    orden.fechaComienza,
                                                    orden.fechaPrepara,
                                                    orden.fechaConduce,
                                                    orden.fechaLlegadaLugar,
                                                    orden.fechaIniciaAtencion,
                                                    orden.fechaFinAtencion,
                                                    orden.fechaFinOrden,
                                                    orden.lugarComienza,
                                                    orden.lugarConduce,
                                                    orden.lugarLlegadaLugar,
                                                    orden.lugarIniciaAtencion,
                                                    orden.lugarFinAtencion,
                                                    orden.lugarFinOrden
                                                    from orden_historial orden
                                                    WHERE orden.Id = " . $_GET["Id"]);


    $row = mysqli_fetch_array($rs);
    $sql = "DELETE FROM tem_orden  ";
    mysqli_query($cn, $sql);


    if ($row["fechaComienza"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaComienza"] . ",'INICIO DE ORDEN','" . $row["lugarComienza"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaPrepara"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaPrepara"] . ",'INICIO DE PREPARACION','" . $row["lugarComienza"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaConduce"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaConduce"] . ",'INICIO DE CONDUCCION AL LUGAR DE TRABAJO','" . $row["lugarConduce"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaLlegadaLugar"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaLlegadaLugar"] . ",'LLEGADA AL LUGAR','" . $row["lugarLlegadaLugar"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }
    if ($row["fechaIniciaAtencion"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaIniciaAtencion"] . ",'INICIO DE SERVICIO','" . $row["lugarIniciaAtencion"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaFinAtencion"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaFinAtencion"] . ",'FIN DE SERVICIO','" . $row["lugarFinAtencion"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaFinOrden"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaFinOrden"] . ",'FIN DE ORDEN DE SERVICIO','" . $row["lugarFinOrden"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }



    $rs = mysqli_query($cn, "Select orden.Id,
                                                    orden.Codigo,
                                                    orden.FechaApertura,
                                                    orden.fechaComienza,
                                                    orden.fechaPrepara,
                                                    orden.fechaConduce,
                                                    orden.fechaLlegadaLugar,
                                                    orden.fechaIniciaAtencion,
                                                    orden.fechaFinAtencion,
                                                    orden.fechaFinOrden,
                                                    orden.lugarComienza,
                                                    orden.lugarConduce,
                                                    orden.lugarLlegadaLugar,
                                                    orden.lugarIniciaAtencion,
                                                    orden.lugarFinAtencion,
                                                    orden.lugarFinOrden
                                                    from orden_trabajo orden
                                                    WHERE orden.Id = " . $_GET["Id"]);
    $row = mysqli_fetch_array($rs);





    if ($row["fechaComienza"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaComienza"] . ",'INICIO DE ORDEN','" . $row["lugarComienza"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaPrepara"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaPrepara"] . ",'INICIO DE PREPARACION','" . $row["lugarComienza"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaConduce"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaConduce"] . ",'INICIO DE CONDUCCION AL LUGAR DE TRABAJO','" . $row["lugarConduce"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaLlegadaLugar"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaLlegadaLugar"] . ",'LLEGADA AL LUGAR','" . $row["lugarLlegadaLugar"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }
    if ($row["fechaIniciaAtencion"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaIniciaAtencion"] . ",'INICIO DE SERVICIO','" . $row["lugarIniciaAtencion"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaFinAtencion"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaFinAtencion"] . ",'FIN DE SERVICIO','" . $row["lugarFinAtencion"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }

    if ($row["fechaFinOrden"] > 0) {
        $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row["fechaFinOrden"] . ",'FIN DE ORDEN DE SERVICIO','" . $row["lugarFinOrden"] . "')";
        //ECHO  $sql;
        mysqli_query($cn, $sql);
    }


    // RECESOS 
    $rs2 = mysqli_query($cn, "SELECT receso.FechaInicio,
                                                receso.FechaFin,
                                                receso.Observacion,
                                                receso.GeoInicio,
                                                receso.GeoFin
                                                FROM receso receso
                                                WHERE receso.Orden = " . $_GET["Id"]);

    $row_count = mysqli_num_rows($rs);

    if ($row_count > 0) {

        while ($row2 = mysqli_fetch_array($rs2)) {
            if ($row2["FechaInicio"] > 0) {
                $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row2["FechaInicio"] . ",'INICIO RECESO: " . $row2["Observacion"] . "','" . $row2["GeoInicio"] . "')";
                // ECHO $sql;
                mysqli_query($cn, $sql);
            }
            if ($row2["FechaFin"] > 0) {
                $sql = "insert into tem_orden (Orden,Fecha,Descripcion,Lugar)values(" . $row["Id"] . "," . $row2["FechaFin"] . ",'FIN RECESO: " . $row2["Observacion"] . "','" . $row2["GeoFin"] . "')";
                // ECHO $sql;
                mysqli_query($cn, $sql);
            }
        }
    }
    // fin de recesos
    ?>

                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                                    <!-- start recent activity -->
                                                    <ul class="messages">
    <?php
    $rs_temp = mysqli_query($cn, "SELECT tem_orden.Fecha,
                                                           tem_orden.Descripcion,
                                                           tem_orden.Lugar,
                                                           tem_orden.Orden
                                                      FROM tem_orden tem_orden
                                                     WHERE (tem_orden.Orden = " . $_GET["Id"] . ") order by tem_orden.Fecha");

    $row_count_temp = mysqli_num_rows($rs_temp);

    if ($row_count_temp > 0) {
        while ($row_temp = mysqli_fetch_array($rs_temp)) {
            ?>

                                                                <li>

                                                                    <img src="fotos/inicio.png" width="45" height="45">                                                                   
                                                                    <div class="message_date">
                                                                        <h3 class="date text-info"><?php echo date("d/m/Y ", $row_temp["Fecha"]); ?></h3>
                                                                        <h4 class="month"><?php echo 'Hora: ' . date("H:i", $row_temp["Fecha"]); ?></h4>
                                                                    </div>
                                                                    <div class="message_wrapper">
                                                                        <h4 class="heading"><?php echo $row_temp[1]; ?>"</h4>
                                                                        <blockquote class="message"></blockquote>
                                                                        <br />
                                                                        <p class="url">
                                                                            <span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span>
                                                                            <a target="_blank" href="https://www.google.com.sv/maps/place/<?php echo $row_temp[2]; ?>"> Verificar ubicacion</a>
                                                                        </p>
                                                                    </div>
                                                                </li>

            <?php
        }
    }
    ?>



                                                    </ul>
                                                    <!-- end recent activity -->

                                                </div>
                                            </div>
                                        </div>







                                        <!--Rutas-->
                                        <div class="panel">


    <?php
    $rs_temp = mysqli_query($cn, "SELECT tem_orden.Fecha,
                                                           tem_orden.Descripcion,
                                                           tem_orden.Lugar,
                                                           tem_orden.Orden
                                                      FROM tem_orden tem_orden
                                                     WHERE (tem_orden.Orden = " . $_GET["Id"] . ") order by tem_orden.Fecha");

    $row_count_temp = mysqli_num_rows($rs_temp);

    if ($row_count_temp > 0) {
        $i = 1;
        $marcadores = "";
        $numeros_orden = "";
        while ($row_temp = mysqli_fetch_array($rs_temp)) {

            $id_orden = $i;
            $orden = $row_temp['Orden'];
            $fecha = date("d/m/Y ", $row_temp['Fecha']);
            $hora = date("H:i", $row_temp['Fecha']);
            $descripcion = $row_temp['Descripcion'];
            $lugar = $row_temp['Lugar'];



            $descipcion_hora = "Orden: " . $orden . "<br>Id: " . $id_orden . "<br>fecha: " . $fecha . "<br>hora: " . $hora . "<br>desc: " . $descripcion;

            $arrayLugar = explode(",", $lugar, 2);

            $latitud = $arrayLugar[0];
            $longitud = $arrayLugar[1];

            $numeros_orden = $numeros_orden . $id_orden . ",";

            $marcadores = $marcadores . "
																{
																  'id': '$id_orden',
																  'lat': '$latitud',
																  'lng': '$longitud',
																  'descripcion': '$descripcion',
																  'fecha': '$fecha',
																  'hora': '$hora',
																  'descipcion_hora': '$descipcion_hora',
																},";







            $i++;
        }


        $numeros_orden = substr($numeros_orden, 0, -1);

        $marcadores = substr($marcadores, 0, -1);

        //echo $marcadores;
        // mysqli_close($cn);
        ?>

                                                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWTotJ95KbtlV_SY2mUlkz6GJRZigjgd0&callback=initMap"></script>
                                                <script type="text/javascript">
                                                    var ordenes = [
        <?php echo $numeros_orden ?>
                                                    ];

                                                    var markers = [
        <?php echo $marcadores; ?>
                                                    ];

                                                    window.onload = function () {
                                                        var mapOptions = {
                                                            zoom: 6,
                                                            center: new google.maps.LatLng(13.735745, -89.125499),
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                                        };
                                                        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
                                                        var infoWindow = new google.maps.InfoWindow();
                                                        var lat_lng = new Array();
                                                        var latlngbounds = new google.maps.LatLngBounds();
                                                        for (i = 0; i < markers.length; i++) {
                                                            var data = markers[i]
                                                            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                                                            lat_lng.push(myLatlng);
                                                            var marker = new google.maps.Marker({
                                                                position: myLatlng,
                                                                map: map,
                                                                label: "" + ordenes[i] + ""
                                                            });
                                                            latlngbounds.extend(marker.position);
                                                            (function (marker, data) {
                                                                google.maps.event.addListener(marker, "click", function (e) {
                                                                    infoWindow.setContent(data.descipcion_hora);
                                                                    infoWindow.open(map, marker);
                                                                });
                                                            })(marker, data);
                                                        }
                                                        map.setCenter(latlngbounds.getCenter());
                                                        map.fitBounds(latlngbounds);

                                                        //***********ROUTING****************//

                                                        //Intialize the Path Array
                                                        var path = new google.maps.MVCArray();

                                                        //Intialize the Direction Service
                                                        var service = new google.maps.DirectionsService();

                                                        //Set the Path Stroke Color
                                                        var poly = new google.maps.Polyline({map: map, strokeColor: '#4986E7'});

                                                        //Loop and Draw Path Route between the Points on MAP
                                                        for (var i = 0; i < lat_lng.length; i++) {
                                                            if ((i + 1) < lat_lng.length) {
                                                                var src = lat_lng[i];
                                                                var des = lat_lng[i + 1];
                                                                path.push(src);
                                                                poly.setPath(path);
                                                                service.route({
                                                                    origin: src,
                                                                    destination: des,
                                                                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                                                                }, function (result, status) {
                                                                    if (status == google.maps.DirectionsStatus.OK) {
                                                                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                                                            path.push(result.routes[0].overview_path[i]);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                </script>



        <?php
    }
    ?>

                                            <a class="panel-heading collapsed" class="panel-collapse collapse in" role="tab" id="heading4" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                <h4 class="panel-title">Rutas</h4>
                                            </a>
                                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">



                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">



                                                    <div id="dvMap" style="width: 1000px; height: 500px">
                                                    </div>



                                                </div>
                                            </div>
                                        </div>











                                        <div class="panel">
                                            <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <h4 class="panel-title">Fotos</h4>
                                            </a>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <p><strong>Diagnóstico de la falla</strong></p>
    <?php
    $rs2 = mysqli_query($cn, "SELECT fotos.Foto  FROM fotos WHERE Tipo = 1 and fotos.Orden = " . $_GET["Id"]);
    $foto_count = mysqli_num_rows($rs2);
    if ($foto_count > 0) {
        while ($row = mysqli_fetch_array($rs2)) {
            ?>

                                                            <div class="col-md-55">
                                                                <div class="thumbnail">
                                                                    <div class="image view view-first">
                                                                        <a target="_blank" href="foto_ver.php?foto=<?php echo $row[0]; ?>">
                                                                            <img style="width: 100%; display: block;" src="imagenes/<?php echo $row[0]; ?>" alt="image" />
                                                                        </a>

                                                                    </div>
                                                                    <div class="caption">
                                                                        <p></p>
                                                                    </div>
                                                                </div>
                                                            </div>
            <?php
        }
    }
    ?>

                                                </div>



                                                <div class="panel-body">
                                                    <p><strong>Condiciones de las piezas o componentes</strong></p>
    <?php
    $rs = mysqli_query($cn, "SELECT fotos.Foto  FROM fotos WHERE Tipo = 2 and fotos.Orden = " . $_GET["Id"]);
    while ($row = mysqli_fetch_array($rs)) {
        ?>

                                                        <div class="col-md-55">
                                                            <div class="thumbnail">
                                                                <div class="image view view-first">
                                                                    <a target="_blank" href="foto_ver.php?foto=<?php echo $row[0]; ?>">
                                                                        <img style="width: 100%; display: block;" src="imagenes/<?php echo $row[0]; ?>" alt="image" />
                                                                    </a>

                                                                </div>
                                                                <div class="caption">
                                                                    <p></p>
                                                                </div>
                                                            </div>
                                                        </div>
        <?php
    }
    ?>

                                                </div>



                                                <div class="panel-body">
                                                    <p><strong>Descripción complementaria del servicio</strong></p>
    <?php
    $rs = mysqli_query($cn, "SELECT fotos.Foto  FROM fotos WHERE Tipo = 3 and fotos.Orden = " . $_GET["Id"]);
    while ($row = mysqli_fetch_array($rs)) {
        ?>

                                                        <div class="col-md-55">
                                                            <div class="thumbnail">
                                                                <div class="image view view-first">
                                                                    <a target="_blank" href="foto_ver.php?foto=<?php echo $row[0]; ?>">
                                                                        <img style="width: 100%; display: block;" src="imagenes/<?php echo $row[0]; ?>" alt="image" />
                                                                    </a>

                                                                </div>
                                                                <div class="caption">
                                                                    <p></p>
                                                                </div>
                                                            </div>
                                                        </div>
        <?php
    }
    ?>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end of accordion -->


                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>







    </body>


    <script type="text/javascript">

                                                    $(document).ready(function () {
                                                        verificarEstado();

                                                        $('#btn_modificar').click(function () {
                                                            if (document.getElementById('Id').value == '') {
                                                                alert('Error!, este registro no existe');
                                                                return false;
                                                            }
                                                            if (document.getElementById('FechaProgramada').value == '') {
                                                                alert('Error!, este se debe seleccionar una fecha');
                                                                return false;
                                                            }

                                                            if (confirm('Esta seguro que desea guardar este Registro?')) {
                                                                $.post('orden_asignar_guardar.php',
                                                                        {
                                                                            Tipo: '2',
                                                                            Id: document.getElementById('Id').value,
                                                                            FechaProgramada: document.getElementById('FechaProgramada').value,
                                                                            Tecnico: document.getElementById('Tecnico').value,
                                                                            Estado: document.getElementById('Estado').value,
                                                                            Nivel: document.getElementById('Nivel').value,
                                                                            Supervisor: document.getElementById('Supervisor').value,

                                                                        },
                                                                        function (data, status) {
                                                                            $('#targetDiv').html(data);
                                                                            // alert(data);
                                                                        });

                                                            }
                                                        });
                                                        function verificarEstado()
                                                        {
                                                            if (document.getElementById('Rol').value > '2')
                                                            {
                                                                $("#Tecnico").prop('disabled', true);
                                                                $("#Supervisor").prop('disabled', true);
                                                                $("#FechaProgramada").prop('disabled', true);
                                                                $("#Estado").prop('disabled', true);
                                                                $("#Nivel").prop('disabled', true);
                                                                $("#btn_modificar").prop('disabled', true);
                                                            } else if (document.getElementById('Estado').value > '2' && document.getElementById('Estado').value <= '9' && document.getElementById('Rol').value == '2')
                                                            {

                                                                $("#Tecnico").prop('disabled', true);
                                                                $("#Supervisor").prop('disabled', true);
                                                                $("#FechaProgramada").prop('disabled', true);
                                                                $("#Estado").prop('disabled', true);
                                                                $("#Nivel").prop('disabled', true);
                                                                $("#btn_modificar").prop('disabled', true);
                                                            }
                                                        }



                                                    });


    </script>



    </html>
    <?php
    mysqli_close($cn);
}
?>

