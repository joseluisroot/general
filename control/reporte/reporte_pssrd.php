
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!-- Bootstrap -->
        <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../../build/css/custom.min.css" rel="stylesheet">

        <script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>MNTSCCAL   Call Reporting<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Cust Number(+)</label>
                                <div class="col-sm-10">
                                    <input type="text" id="CustNumber" autofocus="true" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Division</label>
                                <div class="col-sm-10">
                                    <input type="text"  id="Division" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Salesman Type</label>
                                <div class="col-sm-10">
                                    <input type="text"  id="SalesmanType" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Fecha visita</label>
                                <div class="col-sm-10">
                                    <input type="date"  id="FechaVisita" class="form-control"   >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Fecha de próxima visita</label>
                                <div class="col-sm-10">
                                    <input type="date"  id="FechaPróximaVisita" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">Probabilidad de compra</label>
                                <div class="col-sm-10">
                                    <select  id="ProbabilidadCompra"  class="form-control">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div>
                            </div>

                              <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">TipoVisita</label>
                                <div class="col-sm-10">
                                    <input type="text"  id="TipoVisita" class="form-control">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">MEDICION</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="Id"  class="form-control" value="<?php echo $row[0]; ?>"   >
                                    <select name="Medicion" id="Medicion" class="form-control">
                                        <option>SELECCIONAR</option>
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM medicion");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">UNIDAD MEDIDA</label>
                                <div class="col-sm-10">

                                    <div id="unidad_medida">

                                    </div>


                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">SISTEMA</label>
                                <div class="col-sm-10">

                                    <select name="Sistema" id="Sistema" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM sistema");
                                        echo "SELECT * FROM sistema";
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1] . ' - ' . $row2[2]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">COMPONENTE</label>
                                <div class="col-sm-10">

                                    <select name="Componente" id="Componente" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM componente_principal");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>




                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">ESPECIFICADO</label>
                                <div class="col-sm-10">

                                    <input type="text"  name="Especificado" id="Especificado" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">RESULTADO</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="Resultado" id="Resultado" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>



                            <div class="form-group" align="center">
                                <input type="button" id="btn_agregar" value="  Agregar  " class="btn btn-success">
                                <input type="button" id="btn_mostrar" value="  Mostrar  " class="btn btn-primary">


                                <a href="../orden_detalle.php?Id=<?php echo $_GET["Id"]; ?>"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                            </div>

                            <div class="box-footer" id="targetDiv">

                            </div>



                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#btn_modificar').click(function () {

                                        $.post('orden_guardar.php',
                                                {
                                                    Id: document.getElementById('Id').value,
                                                    Tecnico: document.getElementById('Tecnico').value,
                                                    FechaProgramada: document.getElementById('FechaProgramada').value,
                                                    Estado: document.getElementById('Estado').value,

                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });
                                    });
                                });
                            </script>




                        </form>



                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
