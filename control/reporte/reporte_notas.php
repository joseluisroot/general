<?php
if (isset($_GET["Id"])) {
    include('../../conexion/conexion.php');
    $rs = mysqli_query($cn, "SELECT orden.Id,
       orden.Codigo AS CodOrden,
       cliente.Codigo AS CodCliente,
       cliente.Nombre AS Cliente,
       tecnico.Codigo AS CodTecnico,
       tecnico.Nombre AS Tecnico,
       marca.Codigo AS CodMarca,
       marca.Nombre AS Marca,
       orden.FechaApertura,
       orden.Descripcion,
       orden.Serie,
       orden.Modelo,
       estado.Descripcion AS Estado,
       estado.Id AS IdEstado,
       orden.FechaProgramada,
       tecnico.Id AS IdTecnico
  FROM (((orden_trabajo orden
          INNER JOIN marca marca ON (orden.Marca = marca.Id))
         INNER JOIN cliente cliente ON (orden.Cliente = cliente.Id))
        LEFT JOIN tecnico tecnico ON (orden.Tecnico = tecnico.Id))
       INNER JOIN estado estado ON (orden.Estado = estado.Id) where orden.Id = " . $_GET["Id"]);


    $row_count = mysqli_num_rows($rs);
    $row = mysqli_fetch_array($rs);
    ?>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">

    <script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Reporte de notas<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">MEDICION</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="Id"  class="form-control" value="<?php echo $row[0]; ?>"   >
                                    <select name="Medicion" id="Medicion" class="form-control">
                                        <option>SELECCIONAR</option>
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM medicion");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">UNIDAD MEDIDA</label>
                                <div class="col-sm-10">

                                    <div id="unidad_medida">

                                    </div>


                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">SISTEMA</label>
                                <div class="col-sm-10">

                                    <select name="Sistema" id="Sistema" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM sistema");
                                        echo "SELECT * FROM sistema";
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1] . ' - ' . $row2[2]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">COMPONENTE</label>
                                <div class="col-sm-10">

                                    <select name="Componente" id="Componente" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM componente_principal");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>




                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">ESPECIFICADO</label>
                                <div class="col-sm-10">

                                    <input type="text"  name="Especificado" id="Especificado" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail2" class="col-sm-2 control-label">RESULTADO</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="Resultado" id="Resultado" class="form-control"   onBlur="this.value = this.value.toUpperCase();"  >
                                </div>
                            </div>



                            <div class="form-group" align="center">
                                <input type="button" id="btn_agregar" value="  Agregar  " class="btn btn-success">
                                <input type="button" id="btn_mostrar" value="  Mostrar  " class="btn btn-primary">


                                <a href="../orden_detalle.php?Id=<?php echo $_GET["Id"]; ?>"> <input type="button"  value="  Regresar  " class="btn btn-warning"></a>

                            </div>

                            <div class="box-footer" id="targetDiv">

                            </div>



                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#btn_modificar').click(function () {

                                        $.post('orden_guardar.php',
                                                {
                                                    Id: document.getElementById('Id').value,
                                                    Tecnico: document.getElementById('Tecnico').value,
                                                    FechaProgramada: document.getElementById('FechaProgramada').value,
                                                    Estado: document.getElementById('Estado').value,

                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });
                                    });
                                });
                            </script>




                        </form>



                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery -->
        <script src="../../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../../build/js/custom.min.js"></script>

        <!-- Ajax -->
        <script src="../js/ajax.js"></script>



        <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#Medicion').change(function () {
                                        sendQueryToSelect('unidad_medida.php?medicion=' + document.getElementById('Medicion').value, 'unidad_medida')
                                    });


                                    $('#btn_agregar').click(function () {

                                        $.post('orden_nota_guardar.php',
                                                {
                                                    id_orden: document.getElementById('Id').value,
                                                    medicion: document.getElementById('Medicion').value,
                                                    unidad: document.getElementById('Unidad').value,
                                                    sistema: document.getElementById('Sistema').value,
                                                    componente: document.getElementById('Componente').value,
                                                    especificado: document.getElementById('Especificado').value,
                                                    resultado: document.getElementById('Resultado').value,
                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });
                                    });


                                    $('#btn_mostrar').click(function () {

                                        $.post('orden_nota_mostrar.php',
                                                {
                                                    id_orden: document.getElementById('Id').value,
                                                },
                                                function (data, status) {
                                                    $('#targetDiv').html(data);
                                                    //alert(data);
                                                });
                                    });
                                });
        </script>


    </body>
    </html>
    <?php
}
?>
