<?php
include('../conexion/conexion_admin.php');

$rs = mysqli_query($cn, "SELECT orden_trabajo.Id,
       orden_trabajo.Codigo,
       cliente.Codigo as ClienteCodigo ,
       cliente.Nombre as ClienteNombre,
       marca.Codigo as MarcaCodigo,
       marca.Nombre as MarcaNombre,
       orden_trabajo.FechaApertura,
       orden_trabajo.Descripcion,
       orden_trabajo.Serie,
       orden_trabajo.Modelo,
       tecnico.Codigo as TecnicoCodigo,
       tecnico.Nombre as TecnicoNombre,
       date_format(orden_trabajo.FechaProgramada, '%d/%m/%Y') as FechaProgramada,
       date_format(orden_trabajo.FechaApertura, '%d/%m/%Y') as FechaApertura,
       orden_trabajo.Estado       
  FROM ((`general`.orden_trabajo orden_trabajo
         INNER JOIN `general`.tecnico tecnico
            ON (orden_trabajo.Tecnico = tecnico.Id))
        INNER JOIN `general`.cliente cliente
           ON (orden_trabajo.Cliente = cliente.Id))
       INNER JOIN `general`.marca marca ON (orden_trabajo.Marca = marca.Id)
 WHERE (orden_trabajo.Id = " . $_GET["Id"] . ")");
$row = mysqli_fetch_array($rs);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-wysiwyg -->
        <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
        <!-- Switchery -->
        <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
        <!-- starrr -->
        <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../build/css/custom.min.css" rel="stylesheet">
        
        <!-- Estilo chocen  -->
        <link href="../vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

        
<!--        <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>-->

        <style type="text/css">
            .capa{z-index:10; width:1000%; height:1000%;text-align:center; opacity:.50; background-color:#000;display:none; position:absolute;}
            .btnsubir{ display:none; width:100px; height:30px; background-color:#EA3D0B; color:#FFF;}
            .imgajax{ max-width:100px;}
            .btnupload{ width:300px; height:40px;}
        </style>
        <script language="javascript" src="js/jquery-1.7.2.min.js"></script>

        <!-- libreria jquery -->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
        <!-- cargar.js -->
        <script type="text/javascript" src="js/cargar.js"></script>

    </head>
    <body>
        <div name="frmSubir" id="frmSubir" class="form-horizontal form-label-left">
            
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Referencias del servicio </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo reporte <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select id="tipo_reporte" class="form-control">
                                    <?php
                                    $rs2 = mysqli_query($cn, "SELECT * FROM tipo_reporte");
                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                        ?>
                                        <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                        <?php
                                    }
                                    ?>
                                </select>


                            </div>
                        </div>
                        <div class="x_content">


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Código del distribuidor <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="Distribuidor" id="Distribuidor" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM distribuidor");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1] . ' - ' . $row2[2]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Orden de trabajo<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="IdOrden" id="IdOrden" value="<?php echo $row[0]; ?>">
                                    <input type="text" name="Orden" id="Orden" readonly="true" value="<?php echo $row[1]; ?>"   class="form-control"   onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha del servicio<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="FechaServicio" id="FechaServicio" readonly="true" value="<?php echo $row[12]; ?>"   class="form-control"   onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Código del empleado<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="CodigoEmpleado" id="CodigoEmpleado" readonly="true" value="<?php echo $row[10] . ' - ' . $row[11]; ?>"   class="form-control"   onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Instrucciones del servicio <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" rows="4" id="Instrucciones" readonly="true"><?php echo $row[7]; ?></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Generales del cliente</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cliente<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="CodigoCliente" readonly="" id="CodigoCliente" value="<?php echo $row[2] . ' - ' . $row[3]; ?>"   class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Modelo" readonly="" id="Modelo" value="<?php echo $row[9]; ?>"   class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Número de serie<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Serie" readonly="" id="Serie" value="<?php echo $row[8]; ?>"   class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="Horometro" type="checkbox" class="js-switch"  />  Horometro en funcionanmiento
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Horas <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" min="0" step="0.01" name="Horas" id="Horas" value="0"   class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Millas <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" min="0" step="0.01" name="Millas" id="Millas" value="0"     class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kilometros <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" min="0" step="0.01" name="Kilometros" id="Kilometros" value="0"     class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ubicación del equipo<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Ubicacion" id="Ubicacion"    class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre de contacto<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="NombreContacto" id="NombreContacto"    class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-align-left"></i> Datos Garantía</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel-title">Garantia</h4>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Número de pieza que falló<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="Pieza" id="Pieza"  class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripción de la pieza<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="Descripcion" id="Descripcion"   class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cantidad<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" name="Cantidad" id="Cantidad"  min="0" value="0" class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Código descriptivo <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <select name="Descriptivo" id="Descriptivo" class="form-control">
                                                    <?php
                                                    $rs2 = mysqli_query($cn, "SELECT * FROM sistema");
                                                    while ($row2 = mysqli_fetch_array($rs2)) {
                                                        ?>
                                                        <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1] . ' - ' . $row2[2]; ?></option> 
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Número de grupo en que se halla la pieza<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="NumeroPieza" id="NumeroPieza" value="0"   class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripción del grupo<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="DescripcionGrupo" id="DescripcionGrupo" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">¿A causa de la avería quedó el producto inservible?<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select  name="averia" id="averia" class="form-control col-md-7 col-xs-12">
                                                    <option value="1">Si</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end of accordion -->


                        </div>
                    </div>
                </div>

            </div>



            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Detalles del servicio</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Componentes principales  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <select name="ComponentePrincipal" id="ComponentePrincipal" class="form-control selectpicker" data-live-search="true" title="Seleccionar..." >
                                        
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM componente_principal");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sub-sistemas  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="Subsistema" id="Subsistema" class="form-control selectpicker" data-live-search="true" title="Seleccionar..." >
                                        
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM sub_sistema");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Síntomas reportados<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="SistemaReportado" id="SistemaReportado"    class="form-control col-md-7 col-xs-12"  onBlur="this.value = this.value.toUpperCase();" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Selección de componentes y operaciones<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Operaciones" id="Operaciones"     class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pruebas realizadas<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="PruebasRealizadas" id="PruebasRealizadas"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Calibraciones y ajustes realizados<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="AjustesRealizados" id="AjustesRealizados"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mediciones obtenidas<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Mediciones" id="Mediciones"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Análisis de fluidos<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select  name="Fluidos" id="Fluidos" class="form-control col-md-7 col-xs-12">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Códigos de fallas<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="CodigosFallas" id="CodigosFallas"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Diagnóstico de la falla<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="DiagonosticoFallas" id="DiagnosticoFallas"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            
                            <form id="frmPicDiagnostico" enctype="multipart/form-data" method="POST">
                                <input id="IdOrden" name="IdOrden" type="hidden" value="<?= $_GET["Id"] ?>"/>
                                <input id="tipo" name="tipo" type="hidden" value="1"/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fotos<span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" id="foto1" name="foto1" accept="image/*" capture="camera"  />
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button data-tipo="1" type="submit" class="btn_enviar" id="btn_enviar" name="btn_enviar"  class="btn btn-app" ><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <section id="mensaje1">

                                        </section>
                                    </div>
                                </div>
                            </form>
                                                       
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Condiciones de las piezas o componentes <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="CondicionesTrabajo" id="CondicionesTrabajo" class="form-control">
                                        <?php
                                        $rs2 = mysqli_query($cn, "SELECT * FROM condicion_pieza");
                                        while ($row2 = mysqli_fetch_array($rs2)) {
                                            ?>
                                            <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                                                        
                            <form id="frmPicCondiciones" enctype="multipart/form-data" method="POST">
                                <input id="IdOrden" name="IdOrden" type="hidden" value="<?= $_GET["Id"] ?>"/>
                                <input id="tipo" name="tipo" type="hidden" value="2"/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fotos<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="foto2" id="foto2" accept="image/*" capture="camera"  />
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button data-tipo="2" type="submit" class="btn_enviar" id="btn_enviar2"   class="btn btn-app" ><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <section id="mensaje2">

                                        </section>
                                    </div>
                                </div>
                            </form>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripción complementaria del servicio<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Complementaria" id="Complementaria"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            
                            <form id="frmPicComplemento" enctype="multipart/form-data" method="POST" >
                                <input id="IdOrden" name="IdOrden" type="hidden" value="<?= $_GET["Id"] ?>"/>
                                <input id="tipo" name="tipo" type="hidden" value="3"/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fotos<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="foto3" id="foto3" accept="image/*" capture="camera" />
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button data-tipo="3" type="submit" class="btn_enviar" id="btn_enviar3" name="btn_enviar3"  class="btn btn-app" ><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <section id="mensaje3">

                                        </section>
                                    </div>
                                </div>
                            </form>
                            
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Condiciones de Servicio <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="CondicionesServicio" id="CondicionesServicio" class="form-control">
                                        <?php
                                        $condicionesSer = mysqli_query($cn, "SELECT * FROM condicion_servicio");
                                        while ($condicionesSerRow = mysqli_fetch_array($condicionesSer)) {
                                            ?>
                                            <option value="<?php echo $condicionesSerRow[0]; ?>"><?php echo $condicionesSerRow[1]; ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Condiciones de Equipo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <select name="CondicionesEquipo" id="CondicionesEquipo" class="form-control">
                                        <?php
                                        $condicionesEquipo = mysqli_query($cn, "SELECT * FROM condicion_equipo");
                                        while ($condicionesEquipoRow = mysqli_fetch_array($condicionesEquipo)) {
                                            ?>
                                            <option value="<?php echo $condicionesEquipoRow[0]; ?>"><?php echo $condicionesEquipoRow[1]; ?></option> 
                                            <?php
                                        }
                                        
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Observaciones<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="Observaciones" id="Observaciones"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            
                            


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <input type="hidden" value="" id="idReporteGenerado" />
                                    <a href="orden_detalle.php?Id=<?php echo $_GET["Id"]; ?>"><button class="btn btn-primary" type="button">Regresar</button></a>
                                    <a class="btn btn-success" data-id="<?php echo $_GET["Id"]; ?>" id="guardarInformacionReporte">Aceptar</a>
                                    <a class="btn btn-warning" data-id="<?php echo $_GET["Id"]; ?>" id="modificarInformacionReporte">Modificar</a>
                                    <a class="btn btn-success" data-id="<?php echo $_GET["Id"]; ?>" id="guardarModificacionInformacionReporte">Aceptar</a>
                                    <a class="btn btn-success" data-id="<?php echo $_GET["Id"]; ?>" id="generarReporte">Generar Reporte</a>
                                    <a class="btn btn-success" data-id="<?php echo $_GET["Id"]; ?>" data-recurso="0" id="descargarReporte">Descargar Reporte</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="targetDiv">

            </div>



            <script>
                $(document).ready(function () {
                    
                    $('#generarReporte').hide();
                    $('#descargarReporte').hide();
                    $('#modificarInformacionReporte').hide();
                    $('#guardarModificacionInformacionReporte').hide();
                    
                    recuperarInfoReporte()

                    $('#guardarInformacionReporte').click(function(e){
                        e.preventDefault();
                       
                        if($('#Horometro').is(':checked')){
                              
                            if(parseInt($('#Horas').val())==0 || parseInt($('#Horas').val())<0){
                                alert('Proporcionar la hora es requerido');
                                $('#Horas').focus();
                            }else{
                                guardarReporteInfo();
                            }
                        }   
                        else
                        {
                            guardarReporteInfo();
                        }                     
                        
                        
                    });
                    
                    
                    function guardarReporteInfo(){
                        var reporte={
                                    tipo_reporte:$('#tipo_reporte').val(),
                                    Distribuidor:$('#Distribuidor').val(),
                                    IdOrden:$('#IdOrden').val(),
                                    Horas:$('#Horas').val(),
                                    Millas:$('#Millas').val(),
                                    Kilometros:$('#Kilometros').val(),
                                    Ubicacion:$('#Ubicacion').val(),
                                    NombreContacto:$('#NombreContacto').val(),
                                    Pieza:$('#Pieza').val(),
                                    Descripcion:$('#Descripcion').val(),
                                    Cantidad:$('#Cantidad').val(),
                                    Descriptivo:$('#Descriptivo').val(),
                                    NumeroPieza:$('#NumeroPieza').val(),
                                    DescripcionGrupo:$('#DescripcionGrupo').val(),
                                    averia:$('#averia').val(),
                                    ComponentePrincipal:$('#ComponentePrincipal').val(),
                                    Subsistema:$('#Subsistema').val(),
                                    SistemaReportado:$('#SistemaReportado').val(),
                                    Operaciones:$('#Operaciones').val(),
                                    PruebasRealizadas:$('#PruebasRealizadas').val(),
                                    AjustesRealizados:$('#AjustesRealizados').val(),
                                    Mediciones:$('#Mediciones').val(),
                                    Fluidos:$('#Fluidos').val(),
                                    CodigosFallas:$('#CodigosFallas').val(),
                                    DiagnosticoFallas:$('#DiagnosticoFallas').val(),
                                    CondicionesTrabajo:$('#CondicionesTrabajo').val(),
                                    Complementaria:$('#Complementaria').val(),
                                    CondicionesServicio:$('#CondicionesServicio').val(),
                                    CondicionesEquipo:$('#CondicionesEquipo').val(),
                                    Observaciones:$('#Observaciones').val()
                                }

                                $.ajax({
                                    url: 'guardarInfoReporte.php',
                                    type: 'POST',
                                    data:reporte,
                                    success: function (data) {

                                        var res = $.parseJSON(data);

                                        if (res.error == 'false') {
                                            $('#guardarInformacionReporte').hide();
                                            $('#generarReporte').show();

                                            $('#idReporteGenerado').val(res.id);

                                            alert('Información Guardada con éxito');
                                        } else {
                                            alert('Ocurrio un error al guardar la información');
                                        }



                                    }
                                });
                    }
                    
                    function guardarModificacionReporteInfo(){
                        
                        
                        var reporte={
                            id:$('#idReporteGenerado').val(),
                            tipo_reporte:$('#tipo_reporte').val(),
                            Distribuidor:$('#Distribuidor').val(),
                            IdOrden:$('#IdOrden').val(),
                            Horas:$('#Horas').val(),
                            Millas:$('#Millas').val(),
                            Kilometros:$('#Kilometros').val(),
                            Ubicacion:$('#Ubicacion').val(),
                            NombreContacto:$('#NombreContacto').val(),
                            Pieza:$('#Pieza').val(),
                            Descripcion:$('#Descripcion').val(),
                            Cantidad:$('#Cantidad').val(),
                            Descriptivo:$('#Descriptivo').val(),
                            NumeroPieza:$('#NumeroPieza').val(),
                            DescripcionGrupo:$('#DescripcionGrupo').val(),
                            averia:$('#averia').val(),
                            ComponentePrincipal:$('#ComponentePrincipal').val(),
                            Subsistema:$('#Subsistema').val(),
                            SistemaReportado:$('#SistemaReportado').val(),
                            Operaciones:$('#Operaciones').val(),
                            PruebasRealizadas:$('#PruebasRealizadas').val(),
                            AjustesRealizados:$('#AjustesRealizados').val(),
                            Mediciones:$('#Mediciones').val(),
                            Fluidos:$('#Fluidos').val(),
                            CodigosFallas:$('#CodigosFallas').val(),
                            DiagnosticoFallas:$('#DiagnosticoFallas').val(),
                            CondicionesTrabajo:$('#CondicionesTrabajo').val(),
                            Complementaria:$('#Complementaria').val(),
                            CondicionesServicio:$('#CondicionesServicio').val(),
                            CondicionesEquipo:$('#CondicionesEquipo').val(),
                            Observaciones:$('#Observaciones').val()
                        }

                        $.ajax({
                            url: 'guardarModiInfoReporte.php',
                            type: 'POST',
                            data:reporte,
                            success: function (data) {

                                var res = $.parseJSON(data);

                                if (res.error == 'false') {
                                    $('#guardarModificacionInformacionReporte').hide();
                                    $('#generarReporte').show();

                                    alert('Información Guardada con éxito');
                                } else {
                                    alert('Ocurrio un error al guardar la información');
                                }



                            }
                        });
                    }
                    
                    $('#generarReporte').click(function(e){
                         e.preventDefault();
                         
                         var info = {
                             id:$('#generarReporte').data('id'),
                             idReporte: $('#idReporteGenerado').val()                           
                         }
                         
                         $.ajax({
                            url: '../reportes/reporteOrdenTrabajo.php',
                            type: 'POST',
                            data:info,
                            success: function (data) {
                                                                                              
                                var res = $.parseJSON(data);
                                
                                if (res.error == 'false') {
                                    
                                    $('#generarReporte').hide();
                                    $('#descargarReporte').show();
                                    
                                    $('#descargarReporte').attr('data-recurso',res.file);
                                    
                                    $('#descargarReporte').attr('href',window.location.origin+'/general/reportes/'+res.file);
                                    $('#descargarReporte').attr('target', '_blank');
                                    alert('Rerpote Generado');
                                }
                                else{
                                     alert('problemas al generar el reporte');
                                }
                                                                
                                
                            }
                        });
                         
                    });
                    
                    function recuperarInfoReporte(){
                        
                        $.get('infoReporte.php',{id:$('#IdOrden').val()})
                            .done(function(data){

                                var res = $.parseJSON(data);

                                if (res.error == 'false') {
                                    
                                    $('#idReporteGenerado').val(res.idReporte);

                                   if (res.file=='' || res.file=='0'){
                                        $('#guardarInformacionReporte').hide();  
                                        $('#generarReporte').show();
                                        
                                   }else{                                      
                                       
                                       $('#Horas').val(res.dataRerpote.horas);
                                       $('#Millas').val(res.dataRerpote.millas);
                                       $('#Kilometros').val(res.dataRerpote.kilometros);
                                       $('#Ubicacion').val(res.dataRerpote.ubicacionEquipo);
                                       $('#NombreContacto').val(res.dataRerpote.nombreContacto);
                                       
                                       $('#Pieza').val(res.dataRerpote.noPieza);
                                       $('#Descripcion').val(res.dataRerpote.descripcionPieza);
                                       $('#Cantidad').val(res.dataRerpote.cantidad);
                                       $('#Descriptivo').val(res.dataRerpote.codigoDesriptivo);
                                       $('#NumeroPieza').val(res.dataRerpote.numeroGrupoPieza);
                                       $('#DescripcionGrupo').val(res.dataRerpote.descripcionGrupoPieza);
                                       $('#averia').val(res.dataRerpote.averia);
                                       
                                       $('#ComponentePrincipal').val(res.dataRerpote.componentePrincipal);
                                       $('#Subsistema').val(res.dataRerpote.subSistema);
                                       $('#SistemaReportado').val(res.dataRerpote.sintomasReportados);
                                       $('#Operaciones').val(res.dataRerpote.seleccionComponentesOperaciones);
                                       $('#PruebasRealizadas').val(res.dataRerpote.pruebasRealizadas);
                                       $('#AjustesRealizados').val(res.dataRerpote.calibracionesAjustes);
                                       $('#Mediciones').val(res.dataRerpote.mediciones);
                                       $('#Fluidos').val(res.dataRerpote.analisisFluidos);
                                       $('#CodigosFallas').val(res.dataRerpote.codigosFallas);
                                       $('#DiagnosticoFallas').val(res.dataRerpote.diagnosticoFalla);
                                       $('#CondicionesTrabajo').val(res.dataRerpote.condicionesPiezas);
                                       $('#Complementaria').val(res.dataRerpote.descripcionComplementaria);
                                       $('#CondicionesServicio').val(res.dataRerpote.condicionesServicio),
                                       $('#CondicionesEquipo').val(res.dataRerpote.condicionesPieza),
                                       $('#Observaciones').val(res.dataRerpote.observaciones);
                                       
                                       
                                       
                                       

                                        $('#guardarInformacionReporte').hide();                                       
                                        $('#generarReporte').hide();
                                        $('#descargarReporte').show();
                                        
                                        $('#descargarReporte').attr('href',window.location.origin+'/general/reportes/'+res.file);
                                        deshabilitarCampos();
                                        $('#modificarInformacionReporte').show();
                                        
                                        $('.selectpicker').selectpicker('refresh');
                                   }

                                } 

                         });
                        
                    }
                    
                    function deshabilitarCampos(){
                    
                    $("#tipo_reporte").prop('disabled', true);
                    $("#Distribuidor").prop('disabled', true);
                    $("#Horometro").prop('disabled', true);
                    $("#Horas").prop('disabled', true);
                    $("#Millas").prop('disabled', true);
                    $("#Kilometros").prop('disabled', true);
                    $("#Ubicacion").prop('disabled', true);
                    $("#NombreContacto").prop('disabled', true);
                    
                    $("#Pieza").prop('disabled', true);
                    $("#Descripcion").prop('disabled', true);
                    $("#Cantidad").prop('disabled', true);
                    $("#Descriptivo").prop('disabled', true);
                    $("#NumeroPieza").prop('disabled', true);
                    $("#DescripcionGrupo").prop('disabled', true);
                    $("#averia").prop('disabled', true);
                    
                    $("#ComponentePrincipal").prop('disabled', true);                    
                    //$("#ComponentePrincipal").css("cursor", "not-allowed");
                    
                    $("#Subsistema").prop('disabled', true);
                    //$("#Subsistema").css("cursor", "not-allowed");
                    
                    $("#SistemaReportado").prop('disabled', true);
                    
                    $("#Operaciones").prop('disabled', true);
                    $("#PruebasRealizadas").prop('disabled', true);
                    $("#AjustesRealizados").prop('disabled', true);
                    $("#Mediciones").prop('disabled', true);
                    $("#Fluidos").prop('disabled', true);
                    $("#CodigosFallas").prop('disabled', true);
                    $("#DiagnosticoFallas").prop('disabled', true);
                    $("#foto1").prop('disabled', true);
                    
                    $("#CondicionesTrabajo").prop('disabled', true);
                    $("#foto2").prop('disabled', true);
                    
                    $("#Complementaria").prop('disabled', true);
                    
                    $("#foto3").prop('disabled', true);
                    
                    $(".btn_enviar").prop('disabled', true);
                    
                    $('#CondicionesServicio').prop('disabled', true),
                    $('#CondicionesEquipo').prop('disabled', true),
                    
                    $("#Observaciones").prop('disabled', true);
                    $('.selectpicker').selectpicker('refresh');
                }
                
                    function habilitarCampos(){
                    
                        $("#tipo_reporte").prop('disabled', false);
                        $("#Distribuidor").prop('disabled', false);
                        $("#Horometro").prop('disabled', false);
                        $("#Horas").prop('disabled', false);
                        $("#Millas").prop('disabled', false);
                        $("#Kilometros").prop('disabled', false);
                        $("#Ubicacion").prop('disabled', false);
                        $("#NombreContacto").prop('disabled', false);

                        $("#Pieza").prop('disabled', false);
                        $("#Descripcion").prop('disabled', false);
                        $("#Cantidad").prop('disabled', false);
                        $("#Descriptivo").prop('disabled', false);
                        $("#NumeroPieza").prop('disabled', false);
                        $("#DescripcionGrupo").prop('disabled', false);
                        $("#averia").prop('disabled', false);

                        $("#ComponentePrincipal").prop('disabled', false);                    
                        //$("#ComponentePrincipal").css("cursor", "not-allowed");

                        $("#Subsistema").prop('disabled', false);
                        //$("#Subsistema").css("cursor", "not-allowed");

                        $("#SistemaReportado").prop('disabled', false);

                        $("#Operaciones").prop('disabled', false);
                        $("#PruebasRealizadas").prop('disabled', false);
                        $("#AjustesRealizados").prop('disabled', false);
                        $("#Mediciones").prop('disabled', false);
                        $("#Fluidos").prop('disabled', false);
                        $("#CodigosFallas").prop('disabled', false);
                        $("#DiagnosticoFallas").prop('disabled', false);
                        $("#foto1").prop('disabled', false);

                        $("#CondicionesTrabajo").prop('disabled', false);
                        $("#foto2").prop('disabled', false);

                        $("#Complementaria").prop('disabled', false);

                        $("#foto3").prop('disabled', false);

                        $(".btn_enviar").prop('disabled', false);
                        
                        $('#CondicionesServicio').prop('disabled', false),
                        $('#CondicionesEquipo').prop('disabled', false),

                        $("#Observaciones").prop('disabled', false);
                        $('.selectpicker').selectpicker('refresh');
                    }
                    
                    $('#ComponentePrincipal').change(function(){
                        
                        $.get('subSistemas.php',{Id:$('#ComponentePrincipal').val()})
                                .done(function(data){
                                    
                                 
                          
                                  $('#Subsistema').html(data);
                                  
                                  $('.selectpicker').selectpicker('refresh');                                
                                });
                        
                        
                    });
                    
                    
                    $('.selectpicker').selectpicker();
                    
                    $('#modificarInformacionReporte').click(function(){
                        habilitarCampos();
                        $('#modificarInformacionReporte').hide();
                        $('#guardarModificacionInformacionReporte').show();
                        $('#descargarReporte').hide();
                    });

                    $('#guardarModificacionInformacionReporte').click(function(e){                        
                        e.preventDefault();
                                       
                       
                        if($('#Horometro').is(':checked')){
                              
                            if(parseInt($('#Horas').val())==0 || parseInt($('#Horas').val())<0){
                                alert('Proporcionar la hora es requerido');
                                $('#Horas').focus();
                            }else{
                                guardarModificacionReporteInfo();
                                deshabilitarCampos();
                                $('#guardarModificacionInformacionReporte').hide();
                            }
                        }   
                        else
                        {
                            guardarModificacionReporteInfo();
                            deshabilitarCampos();
                        $('#guardarModificacionInformacionReporte').hide();
                        } 
                        
                        
                        
                    });
                                       
                });
            </script>


        </div>
        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../vendors/moment/min/moment.min.js"></script>
        <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap-wysiwyg -->
        <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
        <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
        <script src="../vendors/google-code-prettify/src/prettify.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Switchery -->
        <script src="../vendors/switchery/dist/switchery.min.js"></script>
        <!-- Select2 -->
        <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
        <!-- Parsley -->
        <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
        <!-- Autosize -->
        <script src="../vendors/autosize/dist/autosize.min.js"></script>
        <!-- jQuery autocomplete -->
        <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
        <!-- starrr -->
        <script src="../vendors/starrr/dist/starrr.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
        
        <!-- Seleccionar con autocompletar-->
        <script src="../vendors/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    </body>
</html>
