<?php
include('../conexion/conexion_admin.php');
$host = mysqli_query($cn,"SELECT valor FROM configcorreo WHERE nombre = 'host'");
$puerto = mysqli_query($cn,"SELECT valor FROM configcorreo WHERE nombre = 'puerto'");
$Username = mysqli_query($cn,"SELECT valor FROM configcorreo WHERE nombre = 'Username'");
$Password = mysqli_query($cn,"SELECT valor FROM configcorreo WHERE nombre = 'Password'");
$SMTPSecure = mysqli_query($cn,"SELECT valor FROM configcorreo WHERE nombre = 'SMTPSecure'");

$rsHost = mysqli_fetch_array($host);
$rspuerto = mysqli_fetch_array($puerto);
$rsUsername = mysqli_fetch_array($Username);
$rsPassword = mysqli_fetch_array($Password);
$rsSMTPSecure = mysqli_fetch_array($SMTPSecure);
 mysqli_close($cn);
?>
<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">

<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Configuraciones de Correo <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>


                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                        
                        <div class="box-body">
                            <div class="form-group">
                                <label for="servidor" class="col-sm-2 control-label">Servidor</label>

                                <div class="col-sm-10">
                                    <input type="text" name="servidor" id="servidor" class="form-control" required="true" value="<?= $rsHost[0] ?>"  >
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Usuario" class="col-sm-2 control-label">Usuario</label>

                                <div class="col-sm-10">
                                    <input type="text" name="Usuario" id="Usuario"  class="form-control" required="true" autofocus="true" value="<?= $rsUsername[0] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Password" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="text" name="Password" id="Password"  class="form-control" required="true" value="<?= $rsPassword[0]  ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Puerto" class="col-sm-2 control-label">Puerto</label>
                                <div class="col-sm-10">
                                    <input name="Puerto" id="Puerto"  class="form-control" required="true" value="<?= $rspuerto[0] ?>" >
                                    <span>(465,587,25)</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="box-body">
                            <div class="form-group">
                                <label for="SMTPSecure" class="col-sm-2 control-label">SMTPSecure</label>
                                <div class="col-sm-10">
                                    <input name="SMTPSecure" id="SMTPSecure"  class="form-control" required="true" value="<?= $rsSMTPSecure[0] ?>" >
                                    <span>(tls,ssl)</span>
                                </div>
                            </div>
                        </div>
                        
                        
                         <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button id="btn_aceptar" type="button" class="btn btn-info pull-right">Aceptar</button>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer" id="targetDiv">
                            
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#btn_aceptar').click(function () {
                                    if (document.getElementById('servidor').value == '') {
                                        alert('Error!, Digite un servidor');
                                        return false;
                                    }
                                    if (document.getElementById('Usuario').value == '') {
                                        alert('Error!, Digite un usuario');
                                        return false;
                                    }
                                    if (document.getElementById('Password').value == '') {
                                        alert('Error!, Digite un password');
                                        return false;
                                    }
                                    
                                    $.post('correo_guardar.php',
                                            {
                                                
                                                Servidor: document.getElementById('servidor').value,
                                                Usuario: document.getElementById('Usuario').value,
                                                Password:$('#Password').val(),
                                                Puerto:$('#Puerto').val(),
                                                SMTPsecure:$('#SMTPSecure').val()
                                                

                                            },
                                            function (data, status) {
                                                $('#targetDiv').html(data);
                                                //alert(data);
                                            });


                                });
                            });
                        </script>


                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="clearfix"></div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
</body>
</html>
